package com.talkyoung.ptbpro;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RagexTest {
    @Test
    public void test(){
        String ragex1 = "(?:https):\\/\\/[\\w\\.-]+\\/[\\w]+\\.[\\w]+";
        String ragex3 = "(http|ftp|https):\\/\\/[\\w\\-_]+(\\.[\\w\\-_]+)+([\\w\\-\\.,@?^=%&:/~\\+#]*[\\w\\-\\@?^=%&/~\\+#])?";
        List<String> urlList = new ArrayList<>();
        String needMatchStr = "<p><img src=\"https://personal-technology-blog.oss-cn-shanghai.aliyuncs.com/202002152210301000001.jpg\"></p><p>123123</p><p><img src=\"https://personal-technology-blog.oss-cn-shanghai.aliyuncs.com/202002152210421000002.png\"></p><p><br></p><p>发射点发范德萨</p><p><br></p><p><br></p><p>12312</p>";
        Pattern pattern = Pattern.compile(ragex1);
        Matcher matcher = pattern.matcher(needMatchStr);
        while(matcher.find()) {
            urlList.add(matcher.group());
        }
        System.out.println(urlList);
    }
}
