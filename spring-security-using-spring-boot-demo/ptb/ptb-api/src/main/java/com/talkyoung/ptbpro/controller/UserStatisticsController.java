package com.talkyoung.ptbpro.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author Talkyoung
 * @Date 2020/2/27 22:39
 * @Version 1.0
 */
@RestController
@RequestMapping("/userStatistics")
public class UserStatisticsController {

    private static final Logger logger = LoggerFactory.getLogger(UserStatisticsController.class);

}
