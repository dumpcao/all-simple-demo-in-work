package com.talkyoung.ptbpro.common.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

public class GenerateIdUtil {

    private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");

    private static final AtomicInteger atomicInteger = new AtomicInteger(1000000);

    /**
     * 获取同一秒钟 生成的订单号连续
     *
     * @return 同一秒内订单连续的编号
     *
     * SimpleDateFormat 是线程不安全的,AtomicInteger是线程安全的,所以你只需要处理SimpleDateFormat的线程安全问题
     * 建议用TheadLocal保存SimpleDateFormat对象,可以把synchronized关键字去掉
     * 这个代码不支持集群环境
     */
    public static synchronized String generateIdByAtomic() {
        atomicInteger.getAndIncrement();
        int i = atomicInteger.get();
        String date = simpleDateFormat.format(new Date());
        return date + i;
    }


    /**
     * 创建不连续的订单号
     *
     * @param no
     *            数据中心编号
     * @return 唯一的、不连续订单号
     */
    public static synchronized String getOrderNoByUUID(String no) {
        Integer uuidHashCode = UUID.randomUUID().toString().hashCode();
        if (uuidHashCode < 0) {
            uuidHashCode = uuidHashCode * (-1);
        }
        String date = simpleDateFormat.format(new Date());
        return no + date + uuidHashCode;
    }
}
