package com.talkyoung.ptbpro.common.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @Author Talkyoung
 * @Date 2020/2/25 23:05
 * @Version 1.0
 */
public class DateUtil {

    public static String getDateString(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return simpleDateFormat.format(new Date());
    }
}
