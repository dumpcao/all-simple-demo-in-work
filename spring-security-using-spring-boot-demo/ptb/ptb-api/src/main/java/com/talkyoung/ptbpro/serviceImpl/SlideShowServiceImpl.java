package com.talkyoung.ptbpro.serviceImpl;

import com.talkyoung.ptbpojo.entity.SlideShow;
import com.talkyoung.ptbpojo.entity.SlideShowExample;
import com.talkyoung.ptbpojo.mapper.SlideShowMapper;
import com.talkyoung.ptbpro.service.SlideShowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SlideShowServiceImpl implements SlideShowService {

    @Autowired
    SlideShowMapper slideShowMapper;

    @Override
    public int countByExample(SlideShowExample example) {
        return slideShowMapper.countByExample(example);
    }

    @Override
    public int deleteByExample(SlideShowExample example) {
        return slideShowMapper.deleteByExample(example);
    }

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return slideShowMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(SlideShow record) {
        return slideShowMapper.insert(record);
    }

    @Override
    public int insertSelective(SlideShow record) {
        return slideShowMapper.insertSelective(record);
    }

    @Override
    public List<SlideShow> selectByExample(SlideShowExample example) {
        return slideShowMapper.selectByExample(example);
    }

    @Override
    public SlideShow selectByPrimaryKey(Integer id) {
        return slideShowMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByExampleSelective(SlideShow record, SlideShowExample example) {
        return slideShowMapper.updateByExampleSelective(record, example);
    }

    @Override
    public int updateByExample(SlideShow record, SlideShowExample example) {
        return slideShowMapper.updateByExample(record, example);
    }

    @Override
    public int updateByPrimaryKeySelective(SlideShow record) {
        return slideShowMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(SlideShow record) {
        return slideShowMapper.updateByPrimaryKey(record);
    }

    @Override
    public List<SlideShow> selectPage(SlideShow record) {
        return slideShowMapper.selectPage(record);
    }
}
