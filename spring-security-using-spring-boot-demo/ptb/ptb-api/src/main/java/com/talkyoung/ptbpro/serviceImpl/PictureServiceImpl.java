package com.talkyoung.ptbpro.serviceImpl;

import com.talkyoung.ptbpojo.entity.Picture;
import com.talkyoung.ptbpojo.entity.PictureExample;
import com.talkyoung.ptbpojo.mapper.PictureMapper;
import com.talkyoung.ptbpro.service.PictureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PictureServiceImpl implements PictureService {
    @Autowired
    PictureMapper pictureMapper;

    @Override
    public int countByExample(PictureExample example) {
        return pictureMapper.countByExample(example);
    }

    @Override
    public int deleteByExample(PictureExample example) {
        return pictureMapper.deleteByExample(example);
    }

    @Override
    public int insert(Picture record) {
        return pictureMapper.insert(record);
    }

    @Override
    public int insertSelective(Picture record) {
        return pictureMapper.insertSelective(record);
    }

    @Override
    public List<Picture> selectByExample(PictureExample example) {
        return pictureMapper.selectByExample(example);
    }

    @Override
    public int updateByExampleSelective(Picture record, PictureExample example) {
        return pictureMapper.updateByExampleSelective(record, example);
    }

    @Override
    public int updateByExample(Picture record, PictureExample example) {
        return pictureMapper.updateByExample(record, example);
    }

    @Override
    public Picture selectByPrimaryKey(String id) {
        return pictureMapper.selectByPrimaryKey(id);
    }
}
