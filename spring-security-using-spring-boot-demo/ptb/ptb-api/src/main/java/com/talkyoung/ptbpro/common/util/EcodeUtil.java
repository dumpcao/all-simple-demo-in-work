package com.talkyoung.ptbpro.common.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

public class EcodeUtil {

    private static BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

    @Autowired
    private static PasswordEncoder passwordEncoder;

    @Autowired
    private static UserDetailsService userDetailsService;

    @Autowired
    private static JwtTokenUtil jwtTokenUtil;

    //根据Bcrypt进行加密
    public static String encodePwd(String password){
        return bCryptPasswordEncoder.encode(password);
    }

    public static void generateToken(String username,String password) {
        UserDetails userDetails = userDetailsService.loadUserByUsername(username);
        String token = null;
        if(userDetails!=null){
            token = jwtTokenUtil.generateToken(userDetails);
            System.out.println("user(admin) token:"+ token);
        }else{
            System.out.println("无法在数据库中查找到用户"+username);
        }
        String artificialPwd = password;
        String generatePwdEncry = passwordEncoder.encode(artificialPwd);
        System.out.println("pws(111111) encryp:"+ generatePwdEncry);
    }
}
