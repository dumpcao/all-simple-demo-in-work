package com.talkyoung.ptbpro.serviceImpl;

import com.talkyoung.ptbpojo.entity.UserPermission;
import com.talkyoung.ptbpojo.entity.UserPermissionExample;
import com.talkyoung.ptbpojo.mapper.UserPermissionMapper;
import com.talkyoung.ptbpro.service.UserPermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author Talkyoung
 * @Date 2userPermissionMapper.2userPermissionMapper./2/29 13:37
 * @Version 1.userPermissionMapper.
 */
@Service
public class UserPermissionServiceImpl implements UserPermissionService {

    @Autowired
    UserPermissionMapper userPermissionMapper;
    @Override
    public int countByExample(UserPermissionExample example) {
        return userPermissionMapper.countByExample(example);
    }

    @Override
    public int deleteByExample(UserPermissionExample example) {
        return userPermissionMapper.deleteByExample(example);
    }

    @Override
    public int deleteByPrimaryKey(Long id) {
        return userPermissionMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(UserPermission record) {
        return userPermissionMapper.insert(record);
    }

    @Override
    public int insertSelective(UserPermission record) {
        return userPermissionMapper.insertSelective(record);
    }

    @Override
    public List<UserPermission> selectByExample(UserPermissionExample example) {
        return userPermissionMapper.selectByExample(example);
    }

    @Override
    public UserPermission selectByPrimaryKey(Long id) {
        return userPermissionMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByExampleSelective(UserPermission record, UserPermissionExample example) {
        return userPermissionMapper.updateByExampleSelective(record, example);
    }

    @Override
    public int updateByExample(UserPermission record, UserPermissionExample example) {
        return userPermissionMapper.updateByExample(record,example);
    }

    @Override
    public int updateByPrimaryKeySelective(UserPermission record) {
        return userPermissionMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(UserPermission record) {
        return userPermissionMapper.updateByPrimaryKey(record);
    }

    @Override
    public List<UserPermission> selectAuthoritiesByUserId(Integer userId) {
        return userPermissionMapper.selectAuthoritiesByUserId(userId);
    }


}
