package com.talkyoung.ptbpro.service;

import com.talkyoung.ptbpojo.entity.SlideShow;
import com.talkyoung.ptbpojo.entity.SlideShowExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SlideShowService {
    int countByExample(SlideShowExample example);

    int deleteByExample(SlideShowExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(SlideShow record);

    int insertSelective(SlideShow record);

    List<SlideShow> selectByExample(SlideShowExample example);

    SlideShow selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") SlideShow record, @Param("example") SlideShowExample example);

    int updateByExample(@Param("record") SlideShow record, @Param("example") SlideShowExample example);

    int updateByPrimaryKeySelective(SlideShow record);

    int updateByPrimaryKey(SlideShow record);

    List<SlideShow> selectPage(SlideShow record);

}
