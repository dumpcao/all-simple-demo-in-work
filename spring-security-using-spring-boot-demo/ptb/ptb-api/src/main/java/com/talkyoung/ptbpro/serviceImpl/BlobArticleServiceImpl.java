package com.talkyoung.ptbpro.serviceImpl;

import com.talkyoung.ptbpojo.entity.BlobArticle;
import com.talkyoung.ptbpojo.entity.BlobArticleExample;
import com.talkyoung.ptbpojo.entity.BlobArticleWithBLOBs;
import com.talkyoung.ptbpojo.mapper.BlobArticleMapper;
import com.talkyoung.ptbpro.service.BlobArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BlobArticleServiceImpl implements BlobArticleService {

    @Autowired
    BlobArticleMapper blobArticleMapper;


    @Override
    public int countByExample(BlobArticleExample example) {
        return blobArticleMapper.countByExample(example);
    }

    @Override
    public int deleteByExample(BlobArticleExample example) {
        return blobArticleMapper.deleteByExample(example);
    }

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return blobArticleMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(BlobArticleWithBLOBs record) {
        return blobArticleMapper.insert(record);
    }

    @Override
    public int insertSelective(BlobArticleWithBLOBs record) {
        return blobArticleMapper.insertSelective(record);
    }

    @Override
    public List<BlobArticleWithBLOBs> selectByExampleWithBLOBs(BlobArticleExample example) {
        return blobArticleMapper.selectByExampleWithBLOBs(example);
    }

    @Override
    public List<BlobArticle> selectByExample(BlobArticleExample example) {
        return blobArticleMapper.selectByExample(example);
    }

    @Override
    public BlobArticleWithBLOBs selectByPrimaryKey(Integer id) {
        return blobArticleMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByExampleSelective(BlobArticleWithBLOBs record, BlobArticleExample example) {
        return blobArticleMapper.updateByExampleSelective(record, example);
    }

    @Override
    public int updateByExampleWithBLOBs(BlobArticleWithBLOBs record, BlobArticleExample example) {
        return blobArticleMapper.updateByExampleWithBLOBs(record, example);
    }

    @Override
    public int updateByExample(BlobArticle record, BlobArticleExample example) {
        return blobArticleMapper.updateByExample(record, example);
    }

    @Override
    public int updateByPrimaryKeySelective(BlobArticleWithBLOBs record) {
        return blobArticleMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKeyWithBLOBs(BlobArticleWithBLOBs record) {
        return blobArticleMapper.updateByPrimaryKeyWithBLOBs(record);
    }

    @Override
    public int updateByPrimaryKey(BlobArticle record) {
        return blobArticleMapper.updateByPrimaryKey(record);
    }
}
