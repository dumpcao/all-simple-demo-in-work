package com.talkyoung.ptbpro.serviceImpl;

import com.talkyoung.ptbpojo.entity.UserStatistics;
import com.talkyoung.ptbpojo.entity.UserStatisticsExample;
import com.talkyoung.ptbpojo.mapper.UserStatisticsMapper;
import com.talkyoung.ptbpro.service.UserStatisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author Talkyoung
 * @Date 2userStatisticsService.2userStatisticsService./2/27 22:37
 * @Version 1.userStatisticsService.
 */

@Service
public class UserStatisticsServiceImpl implements UserStatisticsService {

    @Autowired
    UserStatisticsMapper userStatisticsMapper;
    @Override
    public int countByExample(UserStatisticsExample example) {
        return userStatisticsMapper.countByExample(example);
    }

    @Override
    public int deleteByExample(UserStatisticsExample example) {
        return userStatisticsMapper.deleteByExample(example);
    }

    @Override
    public int deleteByPrimaryKey(Integer userId) {
        return userStatisticsMapper.deleteByPrimaryKey(userId);
    }

    @Override
    public int insert(UserStatistics record) {
        return userStatisticsMapper.insert(record);
    }

    @Override
    public int insertSelective(UserStatistics record) {
        return userStatisticsMapper.insertSelective(record);
    }

    @Override
    public List<UserStatistics> selectByExample(UserStatisticsExample example) {
        return userStatisticsMapper.selectByExample(example);
    }

    @Override
    public UserStatistics selectByPrimaryKey(Integer userId) {
        return userStatisticsMapper.selectByPrimaryKey(userId);
    }

    @Override
    public int updateByExampleSelective(UserStatistics record, UserStatisticsExample example) {
        return userStatisticsMapper.updateByExampleSelective(record,example);
    }

    @Override
    public int updateByExample(UserStatistics record, UserStatisticsExample example) {
        return userStatisticsMapper.updateByExample(record, example);
    }

    @Override
    public int updateByPrimaryKeySelective(UserStatistics record) {
        return userStatisticsMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(UserStatistics record) {
        return userStatisticsMapper.updateByPrimaryKey(record);
    }
}
