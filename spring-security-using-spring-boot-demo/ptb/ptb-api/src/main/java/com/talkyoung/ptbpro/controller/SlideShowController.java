package com.talkyoung.ptbpro.controller;

import com.github.pagehelper.PageHelper;
import com.talkyoung.ptbcommon.CommonResult;
import com.talkyoung.ptbpojo.entity.SlideShow;
import com.talkyoung.ptbpojo.entity.SlideShowExample;
import com.talkyoung.ptbpojo.entity.Student;
import com.talkyoung.ptbpro.service.PictureService;
import com.talkyoung.ptbpro.service.SlideShowService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequestMapping("/slideShow")
@RestController
public class SlideShowController {

    private static Logger logger = LoggerFactory.getLogger(SlideShowController.class);

    @Autowired
    SlideShowService slideShowService;

    @Autowired
    PictureService pictureService;

    @RequestMapping(value = "",method = RequestMethod.GET)
    public CommonResult<List<SlideShow>> selectPage(SlideShow slideShow,
                                                    @RequestParam(value = "pageNum",required = false) Integer pageNum,
                                                    @RequestParam(value = "pageSize",required = false) Integer pageSize){

        slideShowService.selectPage(slideShow);
        return null;
    }

    //找到正在使用的轮播图
    @RequestMapping(value = "/selectDisplay",method = RequestMethod.GET)
    public CommonResult<List<SlideShow>> selectDisplay(){
        PageHelper.startPage(1,4);
        SlideShowExample slideShowExample = new SlideShowExample();
        SlideShowExample.Criteria criteria = slideShowExample.createCriteria();
        criteria.andDeleteFlagEqualTo(1);
        criteria.andTypeEqualTo(1);
        slideShowExample.setOrderByClause("sortno");
        List<SlideShow> slideShows = slideShowService.selectByExample(slideShowExample);
        for(SlideShow slideShow: slideShows){
            slideShow.setPictureUrl(pictureService.selectByPrimaryKey(slideShow.getPictureId()).getSrc());
        }
        return CommonResult.success(slideShows);
    }

}
