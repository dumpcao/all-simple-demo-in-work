package com.talkyoung.ptbpro.controller;

import com.talkyoung.ptbcommon.CommonResult;
import com.talkyoung.ptbpojo.entity.BlobArticle;
import com.talkyoung.ptbpojo.entity.UserArticleThumbUp;
import com.talkyoung.ptbpojo.entity.UserArticleThumbUpKey;
import com.talkyoung.ptbpojo.entity.UserStatistics;
import com.talkyoung.ptbpro.common.util.DateUtil;
import com.talkyoung.ptbpro.common.util.IdGenerateUtil;
import com.talkyoung.ptbpro.service.BlobArticleService;
import com.talkyoung.ptbpro.service.UserArticleThumbUpService;
import com.talkyoung.ptbpro.service.UserStatisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author Talkyoung
 * @Date 2020/2/25 22:47
 * @Version 1.0
 */
@RestController
@RequestMapping("/userArticleThumbUp")
public class UserArticleThumbUpController {

    @Autowired
    UserArticleThumbUpService userArticleThumbUpService;

    @Autowired
    BlobArticleService blobArticleService;

    @Autowired
    UserStatisticsService userStatisticsService;

    @PreAuthorize("hasRole('R_THUMP_UP')")
    @RequestMapping(value = "/delete",method = RequestMethod.GET)
    public CommonResult<String> delete(@RequestParam Integer articleId,@RequestParam Integer userId){
        UserArticleThumbUpKey userArticleThumbUpKey = new UserArticleThumbUpKey();
        userArticleThumbUpKey.setArticleId(articleId);
        userArticleThumbUpKey.setUserId(userId);
        UserArticleThumbUp checkExists = userArticleThumbUpService.selectByPrimaryKey(userArticleThumbUpKey);
        if(checkExists == null){
            return CommonResult.failed("Record not exits,can not delete.");
        }

        BlobArticle blobArticle = blobArticleService.selectByPrimaryKey(articleId);
        if(blobArticle == null){
            return CommonResult.failed("Article not exits,can not delete.");
        }

        blobArticle.setThumbUpTimes(blobArticle.getThumbUpTimes() - 1);

        UserStatistics userStatistics = userStatisticsService.selectByPrimaryKey(userId);
        userStatistics.setSumThumbUpTimes(userStatistics.getSumThumbUpTimes() - 1);

        userArticleThumbUpService.deleteByPrimaryKey(userArticleThumbUpKey);
        blobArticleService.updateByPrimaryKey(blobArticle);
        userStatisticsService.updateByPrimaryKey(userStatistics);

        return CommonResult.success("Delete Success.");
    }

    @PreAuthorize("hasRole('R_THUMP_UP')")
    @RequestMapping(value = "/create",method = RequestMethod.GET)
    public CommonResult<String> create(@RequestParam Integer articleId,@RequestParam Integer userId){
        UserArticleThumbUpKey userArticleThumbUpKey = new UserArticleThumbUpKey();
        userArticleThumbUpKey.setArticleId(articleId);
        userArticleThumbUpKey.setUserId(userId);
        UserArticleThumbUp checkExists = userArticleThumbUpService.selectByPrimaryKey(userArticleThumbUpKey);
        if(checkExists != null){
            return CommonResult.failed("Record already exits,can not create.");
        }

        BlobArticle blobArticle = blobArticleService.selectByPrimaryKey(articleId);
        if(blobArticle == null){
            return CommonResult.failed("Article not exits,can not create.");
        }

        UserArticleThumbUp userArticleThumbUp = new UserArticleThumbUp();
        userArticleThumbUp.setArticleId(articleId);
        userArticleThumbUp.setUserId(userId);
        userArticleThumbUp.setThumpUpTime(DateUtil.getDateString());

        blobArticle.setThumbUpTimes(blobArticle.getThumbUpTimes() + 1);

        UserStatistics userStatistics = userStatisticsService.selectByPrimaryKey(userId);
        userStatistics.setSumThumbUpTimes(userStatistics.getSumThumbUpTimes() + 1);

        userArticleThumbUpService.insert(userArticleThumbUp);
        blobArticleService.updateByPrimaryKey(blobArticle);
        userStatisticsService.updateByPrimaryKey(userStatistics);
        return CommonResult.success("Create Success.");
    }

}
