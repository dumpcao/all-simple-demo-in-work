package com.talkyoung.ptbpro.service;

import com.talkyoung.ptbpojo.entity.UserArticleThumbUp;
import com.talkyoung.ptbpojo.entity.UserArticleThumbUpExample;
import com.talkyoung.ptbpojo.entity.UserArticleThumbUpKey;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author Talkyoung
 * @Date 2020/2/25 22:51
 * @Version 1.0
 */
public interface UserArticleThumbUpService {
    int countByExample(UserArticleThumbUpExample example);

    int deleteByExample(UserArticleThumbUpExample example);

    int deleteByPrimaryKey(UserArticleThumbUpKey key);

    int insert(UserArticleThumbUp record);

    int insertSelective(UserArticleThumbUp record);

    List<UserArticleThumbUp> selectByExample(UserArticleThumbUpExample example);

    UserArticleThumbUp selectByPrimaryKey(UserArticleThumbUpKey key);

    int updateByExampleSelective(@Param("record") UserArticleThumbUp record, @Param("example") UserArticleThumbUpExample example);

    int updateByExample(@Param("record") UserArticleThumbUp record, @Param("example") UserArticleThumbUpExample example);

    int updateByPrimaryKeySelective(UserArticleThumbUp record);

    int updateByPrimaryKey(UserArticleThumbUp record);
}
