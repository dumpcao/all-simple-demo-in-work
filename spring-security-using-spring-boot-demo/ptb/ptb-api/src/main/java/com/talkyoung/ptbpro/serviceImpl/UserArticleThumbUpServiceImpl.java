package com.talkyoung.ptbpro.serviceImpl;

import com.talkyoung.ptbpojo.entity.UserArticleThumbUp;
import com.talkyoung.ptbpojo.entity.UserArticleThumbUpExample;
import com.talkyoung.ptbpojo.entity.UserArticleThumbUpKey;
import com.talkyoung.ptbpojo.mapper.UserArticleThumbUpMapper;
import com.talkyoung.ptbpro.service.UserArticleThumbUpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author Talkyoung
 * @Date 2userArticleThumbUpMapper.2userArticleThumbUpMapper./2/25 22:53
 * @Version 1.userArticleThumbUpMapper.
 */
@Service
public class UserArticleThumbUpServiceImpl implements UserArticleThumbUpService {

    @Autowired
    UserArticleThumbUpMapper userArticleThumbUpMapper;
    @Override
    public int countByExample(UserArticleThumbUpExample example) {
        return userArticleThumbUpMapper.countByExample(example);
    }

    @Override
    public int deleteByExample(UserArticleThumbUpExample example) {
        return userArticleThumbUpMapper.deleteByExample(example);
    }

    @Override
    public int deleteByPrimaryKey(UserArticleThumbUpKey key) {
        return userArticleThumbUpMapper.deleteByPrimaryKey(key);
    }

    @Override
    public int insert(UserArticleThumbUp record) {
        return userArticleThumbUpMapper.insert(record);
    }

    @Override
    public int insertSelective(UserArticleThumbUp record) {
        return userArticleThumbUpMapper.insertSelective(record);
    }

    @Override
    public List<UserArticleThumbUp> selectByExample(UserArticleThumbUpExample example) {
        return userArticleThumbUpMapper.selectByExample(example);
    }

    @Override
    public UserArticleThumbUp selectByPrimaryKey(UserArticleThumbUpKey key) {
        return userArticleThumbUpMapper.selectByPrimaryKey(key);
    }

    @Override
    public int updateByExampleSelective(UserArticleThumbUp record, UserArticleThumbUpExample example) {
        return userArticleThumbUpMapper.updateByExampleSelective(record, example);
    }

    @Override
    public int updateByExample(UserArticleThumbUp record, UserArticleThumbUpExample example) {
        return userArticleThumbUpMapper.updateByExample(record,example);
    }

    @Override
    public int updateByPrimaryKeySelective(UserArticleThumbUp record) {
        return userArticleThumbUpMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(UserArticleThumbUp record) {
        return userArticleThumbUpMapper.updateByPrimaryKey(record);
    }
}
