package com.talkyoung.ptbpro.common.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.atomic.AtomicIntegerArray;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @author ty
 */
public class IdGenerateUtil {

    public static AtomicLong PICTURE_ID = new AtomicLong(1);

    public static String generateId(AtomicLong SEQUENCE){
        return generateId(SEQUENCE,4);
    }

    public static String generateLongId(AtomicLong SEQUENCE){
        return generateId(SEQUENCE,8);
    }

    private static String generateId(AtomicLong SEQUENCE, int sequenceLength){
        String dateStr = getTimeString();
        String seq = String.valueOf(SEQUENCE.addAndGet(1));
        if(sequenceLength ==4){
            if (SEQUENCE.get() > 9999l) {
                SEQUENCE.set(1);
                seq = String.valueOf(SEQUENCE.get());
            }
        }else{
            if (SEQUENCE.get() > 99999999l) {
                SEQUENCE.set(1);
                seq = String.valueOf(SEQUENCE.get());
            }
        }
        return dateStr + "00000000".substring(0,sequenceLength - seq.length()) + seq + "01";
    }

    private static String getTimeString() {
        String timePattren = "yyyyMMddHHmmss";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(timePattren);
        return simpleDateFormat.format(new Date());
    }
}
