package com.talkyoung.ptbpro.service;

import com.talkyoung.ptbpojo.entity.Picture;
import com.talkyoung.ptbpojo.entity.PictureExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PictureService {
    int countByExample(PictureExample example);

    int deleteByExample(PictureExample example);

    int insert(Picture record);

    int insertSelective(Picture record);

    List<Picture> selectByExample(PictureExample example);

    int updateByExampleSelective(@Param("record") Picture record, @Param("example") PictureExample example);

    int updateByExample(@Param("record") Picture record, @Param("example") PictureExample example);

    Picture selectByPrimaryKey(String id);
}
