package com.talkyoung.ptbpojo.entity;

public class UserArticleBrowse extends UserArticleBrowseKey {
    private String browseTime;

    public String getBrowseTime() {
        return browseTime;
    }

    public void setBrowseTime(String browseTime) {
        this.browseTime = browseTime == null ? null : browseTime.trim();
    }
}