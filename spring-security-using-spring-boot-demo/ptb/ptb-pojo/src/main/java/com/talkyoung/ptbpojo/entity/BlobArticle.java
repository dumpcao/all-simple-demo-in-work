package com.talkyoung.ptbpojo.entity;

public class BlobArticle {
    private Integer id;

    private String title;

    private Integer userId;

    private Integer classifyId;

    private Integer isPublic;

    private String createTime;

    private String lastUpdateTime;

    private Integer viewTimes;

    private Integer commentTimes;

    private Integer collectTimes;

    private Integer thumbUpTimes;

    private String pictureId;

    private String nickName;

    private String profilePhoto;

    private String lastLoginTime;

    private String thumbUpStatus;

    private Integer sumViewTimes;

    private Integer sumCommentTimes;

    private Integer sumCollectTimes;

    private Integer sumThumbUpTimes;

    private Integer sumFansNumber;

    private Integer sumFollowerNumber;

    private Integer sumArticleNumber;

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getProfilePhoto() {
        return profilePhoto;
    }

    public void setProfilePhoto(String profilePhoto) {
        this.profilePhoto = profilePhoto;
    }

    public String getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(String lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public String getThumbUpStatus() {
        return thumbUpStatus;
    }

    public void setThumbUpStatus(String thumbUpStatus) {
        this.thumbUpStatus = thumbUpStatus;
    }

    public Integer getSumViewTimes() {
        return sumViewTimes;
    }

    public void setSumViewTimes(Integer sumViewTimes) {
        this.sumViewTimes = sumViewTimes;
    }

    public Integer getSumCommentTimes() {
        return sumCommentTimes;
    }

    public void setSumCommentTimes(Integer sumCommentTimes) {
        this.sumCommentTimes = sumCommentTimes;
    }

    public Integer getSumCollectTimes() {
        return sumCollectTimes;
    }

    public void setSumCollectTimes(Integer sumCollectTimes) {
        this.sumCollectTimes = sumCollectTimes;
    }

    public Integer getSumThumbUpTimes() {
        return sumThumbUpTimes;
    }

    public void setSumThumbUpTimes(Integer sumThumbUpTimes) {
        this.sumThumbUpTimes = sumThumbUpTimes;
    }

    public Integer getSumFansNumber() {
        return sumFansNumber;
    }

    public void setSumFansNumber(Integer sumFansNumber) {
        this.sumFansNumber = sumFansNumber;
    }

    public Integer getSumFollowerNumber() {
        return sumFollowerNumber;
    }

    public void setSumFollowerNumber(Integer sumFollowerNumber) {
        this.sumFollowerNumber = sumFollowerNumber;
    }

    public Integer getSumArticleNumber() {
        return sumArticleNumber;
    }

    public void setSumArticleNumber(Integer sumArticleNumber) {
        this.sumArticleNumber = sumArticleNumber;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getClassifyId() {
        return classifyId;
    }

    public void setClassifyId(Integer classifyId) {
        this.classifyId = classifyId;
    }

    public Integer getIsPublic() {
        return isPublic;
    }

    public void setIsPublic(Integer isPublic) {
        this.isPublic = isPublic;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime == null ? null : createTime.trim();
    }

    public String getLastUpdateTime() {
        return lastUpdateTime;
    }

    public void setLastUpdateTime(String lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime == null ? null : lastUpdateTime.trim();
    }

    public Integer getViewTimes() {
        return viewTimes;
    }

    public void setViewTimes(Integer viewTimes) {
        this.viewTimes = viewTimes;
    }

    public Integer getCommentTimes() {
        return commentTimes;
    }

    public void setCommentTimes(Integer commentTimes) {
        this.commentTimes = commentTimes;
    }

    public Integer getCollectTimes() {
        return collectTimes;
    }

    public void setCollectTimes(Integer collectTimes) {
        this.collectTimes = collectTimes;
    }

    public Integer getThumbUpTimes() {
        return thumbUpTimes;
    }

    public void setThumbUpTimes(Integer thumbUpTimes) {
        this.thumbUpTimes = thumbUpTimes;
    }

    public String getPictureId() {
        return pictureId;
    }

    public void setPictureId(String pictureId) {
        this.pictureId = pictureId == null ? null : pictureId.trim();
    }
}
