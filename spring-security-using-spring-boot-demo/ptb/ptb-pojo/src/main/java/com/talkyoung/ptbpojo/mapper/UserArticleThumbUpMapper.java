package com.talkyoung.ptbpojo.mapper;

import com.talkyoung.ptbpojo.entity.UserArticleThumbUp;
import com.talkyoung.ptbpojo.entity.UserArticleThumbUpExample;
import com.talkyoung.ptbpojo.entity.UserArticleThumbUpKey;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface UserArticleThumbUpMapper {
    int countByExample(UserArticleThumbUpExample example);

    int deleteByExample(UserArticleThumbUpExample example);

    int deleteByPrimaryKey(UserArticleThumbUpKey key);

    int insert(UserArticleThumbUp record);

    int insertSelective(UserArticleThumbUp record);

    List<UserArticleThumbUp> selectByExample(UserArticleThumbUpExample example);

    UserArticleThumbUp selectByPrimaryKey(UserArticleThumbUpKey key);

    int updateByExampleSelective(@Param("record") UserArticleThumbUp record, @Param("example") UserArticleThumbUpExample example);

    int updateByExample(@Param("record") UserArticleThumbUp record, @Param("example") UserArticleThumbUpExample example);

    int updateByPrimaryKeySelective(UserArticleThumbUp record);

    int updateByPrimaryKey(UserArticleThumbUp record);
}