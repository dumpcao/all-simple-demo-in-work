package com.talkyoung.ptbpojo.mapper;

import com.talkyoung.ptbpojo.entity.ArticleClassify;
import com.talkyoung.ptbpojo.entity.ArticleClassifyExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ArticleClassifyMapper {
    int countByExample(ArticleClassifyExample example);

    int deleteByExample(ArticleClassifyExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(ArticleClassify record);

    int insertSelective(ArticleClassify record);

    List<ArticleClassify> selectByExampleWithBLOBs(ArticleClassifyExample example);

    List<ArticleClassify> selectByExample(ArticleClassifyExample example);

    ArticleClassify selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") ArticleClassify record, @Param("example") ArticleClassifyExample example);

    int updateByExampleWithBLOBs(@Param("record") ArticleClassify record, @Param("example") ArticleClassifyExample example);

    int updateByExample(@Param("record") ArticleClassify record, @Param("example") ArticleClassifyExample example);

    int updateByPrimaryKeySelective(ArticleClassify record);

    int updateByPrimaryKeyWithBLOBs(ArticleClassify record);

    int updateByPrimaryKey(ArticleClassify record);
}