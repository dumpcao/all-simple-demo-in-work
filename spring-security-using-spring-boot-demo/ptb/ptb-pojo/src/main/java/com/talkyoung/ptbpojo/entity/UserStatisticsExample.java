package com.talkyoung.ptbpojo.entity;

import java.util.ArrayList;
import java.util.List;

public class UserStatisticsExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public UserStatisticsExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andUserIdIsNull() {
            addCriterion("user_id is null");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNotNull() {
            addCriterion("user_id is not null");
            return (Criteria) this;
        }

        public Criteria andUserIdEqualTo(Integer value) {
            addCriterion("user_id =", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotEqualTo(Integer value) {
            addCriterion("user_id <>", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThan(Integer value) {
            addCriterion("user_id >", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("user_id >=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThan(Integer value) {
            addCriterion("user_id <", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThanOrEqualTo(Integer value) {
            addCriterion("user_id <=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdIn(List<Integer> values) {
            addCriterion("user_id in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotIn(List<Integer> values) {
            addCriterion("user_id not in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdBetween(Integer value1, Integer value2) {
            addCriterion("user_id between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotBetween(Integer value1, Integer value2) {
            addCriterion("user_id not between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andSumViewTimesIsNull() {
            addCriterion("sum_view_times is null");
            return (Criteria) this;
        }

        public Criteria andSumViewTimesIsNotNull() {
            addCriterion("sum_view_times is not null");
            return (Criteria) this;
        }

        public Criteria andSumViewTimesEqualTo(Integer value) {
            addCriterion("sum_view_times =", value, "sumViewTimes");
            return (Criteria) this;
        }

        public Criteria andSumViewTimesNotEqualTo(Integer value) {
            addCriterion("sum_view_times <>", value, "sumViewTimes");
            return (Criteria) this;
        }

        public Criteria andSumViewTimesGreaterThan(Integer value) {
            addCriterion("sum_view_times >", value, "sumViewTimes");
            return (Criteria) this;
        }

        public Criteria andSumViewTimesGreaterThanOrEqualTo(Integer value) {
            addCriterion("sum_view_times >=", value, "sumViewTimes");
            return (Criteria) this;
        }

        public Criteria andSumViewTimesLessThan(Integer value) {
            addCriterion("sum_view_times <", value, "sumViewTimes");
            return (Criteria) this;
        }

        public Criteria andSumViewTimesLessThanOrEqualTo(Integer value) {
            addCriterion("sum_view_times <=", value, "sumViewTimes");
            return (Criteria) this;
        }

        public Criteria andSumViewTimesIn(List<Integer> values) {
            addCriterion("sum_view_times in", values, "sumViewTimes");
            return (Criteria) this;
        }

        public Criteria andSumViewTimesNotIn(List<Integer> values) {
            addCriterion("sum_view_times not in", values, "sumViewTimes");
            return (Criteria) this;
        }

        public Criteria andSumViewTimesBetween(Integer value1, Integer value2) {
            addCriterion("sum_view_times between", value1, value2, "sumViewTimes");
            return (Criteria) this;
        }

        public Criteria andSumViewTimesNotBetween(Integer value1, Integer value2) {
            addCriterion("sum_view_times not between", value1, value2, "sumViewTimes");
            return (Criteria) this;
        }

        public Criteria andSumCommentTimesIsNull() {
            addCriterion("sum_comment_times is null");
            return (Criteria) this;
        }

        public Criteria andSumCommentTimesIsNotNull() {
            addCriterion("sum_comment_times is not null");
            return (Criteria) this;
        }

        public Criteria andSumCommentTimesEqualTo(Integer value) {
            addCriterion("sum_comment_times =", value, "sumCommentTimes");
            return (Criteria) this;
        }

        public Criteria andSumCommentTimesNotEqualTo(Integer value) {
            addCriterion("sum_comment_times <>", value, "sumCommentTimes");
            return (Criteria) this;
        }

        public Criteria andSumCommentTimesGreaterThan(Integer value) {
            addCriterion("sum_comment_times >", value, "sumCommentTimes");
            return (Criteria) this;
        }

        public Criteria andSumCommentTimesGreaterThanOrEqualTo(Integer value) {
            addCriterion("sum_comment_times >=", value, "sumCommentTimes");
            return (Criteria) this;
        }

        public Criteria andSumCommentTimesLessThan(Integer value) {
            addCriterion("sum_comment_times <", value, "sumCommentTimes");
            return (Criteria) this;
        }

        public Criteria andSumCommentTimesLessThanOrEqualTo(Integer value) {
            addCriterion("sum_comment_times <=", value, "sumCommentTimes");
            return (Criteria) this;
        }

        public Criteria andSumCommentTimesIn(List<Integer> values) {
            addCriterion("sum_comment_times in", values, "sumCommentTimes");
            return (Criteria) this;
        }

        public Criteria andSumCommentTimesNotIn(List<Integer> values) {
            addCriterion("sum_comment_times not in", values, "sumCommentTimes");
            return (Criteria) this;
        }

        public Criteria andSumCommentTimesBetween(Integer value1, Integer value2) {
            addCriterion("sum_comment_times between", value1, value2, "sumCommentTimes");
            return (Criteria) this;
        }

        public Criteria andSumCommentTimesNotBetween(Integer value1, Integer value2) {
            addCriterion("sum_comment_times not between", value1, value2, "sumCommentTimes");
            return (Criteria) this;
        }

        public Criteria andSumCollectTimesIsNull() {
            addCriterion("sum_collect_times is null");
            return (Criteria) this;
        }

        public Criteria andSumCollectTimesIsNotNull() {
            addCriterion("sum_collect_times is not null");
            return (Criteria) this;
        }

        public Criteria andSumCollectTimesEqualTo(Integer value) {
            addCriterion("sum_collect_times =", value, "sumCollectTimes");
            return (Criteria) this;
        }

        public Criteria andSumCollectTimesNotEqualTo(Integer value) {
            addCriterion("sum_collect_times <>", value, "sumCollectTimes");
            return (Criteria) this;
        }

        public Criteria andSumCollectTimesGreaterThan(Integer value) {
            addCriterion("sum_collect_times >", value, "sumCollectTimes");
            return (Criteria) this;
        }

        public Criteria andSumCollectTimesGreaterThanOrEqualTo(Integer value) {
            addCriterion("sum_collect_times >=", value, "sumCollectTimes");
            return (Criteria) this;
        }

        public Criteria andSumCollectTimesLessThan(Integer value) {
            addCriterion("sum_collect_times <", value, "sumCollectTimes");
            return (Criteria) this;
        }

        public Criteria andSumCollectTimesLessThanOrEqualTo(Integer value) {
            addCriterion("sum_collect_times <=", value, "sumCollectTimes");
            return (Criteria) this;
        }

        public Criteria andSumCollectTimesIn(List<Integer> values) {
            addCriterion("sum_collect_times in", values, "sumCollectTimes");
            return (Criteria) this;
        }

        public Criteria andSumCollectTimesNotIn(List<Integer> values) {
            addCriterion("sum_collect_times not in", values, "sumCollectTimes");
            return (Criteria) this;
        }

        public Criteria andSumCollectTimesBetween(Integer value1, Integer value2) {
            addCriterion("sum_collect_times between", value1, value2, "sumCollectTimes");
            return (Criteria) this;
        }

        public Criteria andSumCollectTimesNotBetween(Integer value1, Integer value2) {
            addCriterion("sum_collect_times not between", value1, value2, "sumCollectTimes");
            return (Criteria) this;
        }

        public Criteria andSumThumbUpTimesIsNull() {
            addCriterion("sum_thumb_up_times is null");
            return (Criteria) this;
        }

        public Criteria andSumThumbUpTimesIsNotNull() {
            addCriterion("sum_thumb_up_times is not null");
            return (Criteria) this;
        }

        public Criteria andSumThumbUpTimesEqualTo(Integer value) {
            addCriterion("sum_thumb_up_times =", value, "sumThumbUpTimes");
            return (Criteria) this;
        }

        public Criteria andSumThumbUpTimesNotEqualTo(Integer value) {
            addCriterion("sum_thumb_up_times <>", value, "sumThumbUpTimes");
            return (Criteria) this;
        }

        public Criteria andSumThumbUpTimesGreaterThan(Integer value) {
            addCriterion("sum_thumb_up_times >", value, "sumThumbUpTimes");
            return (Criteria) this;
        }

        public Criteria andSumThumbUpTimesGreaterThanOrEqualTo(Integer value) {
            addCriterion("sum_thumb_up_times >=", value, "sumThumbUpTimes");
            return (Criteria) this;
        }

        public Criteria andSumThumbUpTimesLessThan(Integer value) {
            addCriterion("sum_thumb_up_times <", value, "sumThumbUpTimes");
            return (Criteria) this;
        }

        public Criteria andSumThumbUpTimesLessThanOrEqualTo(Integer value) {
            addCriterion("sum_thumb_up_times <=", value, "sumThumbUpTimes");
            return (Criteria) this;
        }

        public Criteria andSumThumbUpTimesIn(List<Integer> values) {
            addCriterion("sum_thumb_up_times in", values, "sumThumbUpTimes");
            return (Criteria) this;
        }

        public Criteria andSumThumbUpTimesNotIn(List<Integer> values) {
            addCriterion("sum_thumb_up_times not in", values, "sumThumbUpTimes");
            return (Criteria) this;
        }

        public Criteria andSumThumbUpTimesBetween(Integer value1, Integer value2) {
            addCriterion("sum_thumb_up_times between", value1, value2, "sumThumbUpTimes");
            return (Criteria) this;
        }

        public Criteria andSumThumbUpTimesNotBetween(Integer value1, Integer value2) {
            addCriterion("sum_thumb_up_times not between", value1, value2, "sumThumbUpTimes");
            return (Criteria) this;
        }

        public Criteria andSumFansNumberIsNull() {
            addCriterion("sum_fans_number is null");
            return (Criteria) this;
        }

        public Criteria andSumFansNumberIsNotNull() {
            addCriterion("sum_fans_number is not null");
            return (Criteria) this;
        }

        public Criteria andSumFansNumberEqualTo(Integer value) {
            addCriterion("sum_fans_number =", value, "sumFansNumber");
            return (Criteria) this;
        }

        public Criteria andSumFansNumberNotEqualTo(Integer value) {
            addCriterion("sum_fans_number <>", value, "sumFansNumber");
            return (Criteria) this;
        }

        public Criteria andSumFansNumberGreaterThan(Integer value) {
            addCriterion("sum_fans_number >", value, "sumFansNumber");
            return (Criteria) this;
        }

        public Criteria andSumFansNumberGreaterThanOrEqualTo(Integer value) {
            addCriterion("sum_fans_number >=", value, "sumFansNumber");
            return (Criteria) this;
        }

        public Criteria andSumFansNumberLessThan(Integer value) {
            addCriterion("sum_fans_number <", value, "sumFansNumber");
            return (Criteria) this;
        }

        public Criteria andSumFansNumberLessThanOrEqualTo(Integer value) {
            addCriterion("sum_fans_number <=", value, "sumFansNumber");
            return (Criteria) this;
        }

        public Criteria andSumFansNumberIn(List<Integer> values) {
            addCriterion("sum_fans_number in", values, "sumFansNumber");
            return (Criteria) this;
        }

        public Criteria andSumFansNumberNotIn(List<Integer> values) {
            addCriterion("sum_fans_number not in", values, "sumFansNumber");
            return (Criteria) this;
        }

        public Criteria andSumFansNumberBetween(Integer value1, Integer value2) {
            addCriterion("sum_fans_number between", value1, value2, "sumFansNumber");
            return (Criteria) this;
        }

        public Criteria andSumFansNumberNotBetween(Integer value1, Integer value2) {
            addCriterion("sum_fans_number not between", value1, value2, "sumFansNumber");
            return (Criteria) this;
        }

        public Criteria andSumFollowerNumberIsNull() {
            addCriterion("sum_follower_number is null");
            return (Criteria) this;
        }

        public Criteria andSumFollowerNumberIsNotNull() {
            addCriterion("sum_follower_number is not null");
            return (Criteria) this;
        }

        public Criteria andSumFollowerNumberEqualTo(Integer value) {
            addCriterion("sum_follower_number =", value, "sumFollowerNumber");
            return (Criteria) this;
        }

        public Criteria andSumFollowerNumberNotEqualTo(Integer value) {
            addCriterion("sum_follower_number <>", value, "sumFollowerNumber");
            return (Criteria) this;
        }

        public Criteria andSumFollowerNumberGreaterThan(Integer value) {
            addCriterion("sum_follower_number >", value, "sumFollowerNumber");
            return (Criteria) this;
        }

        public Criteria andSumFollowerNumberGreaterThanOrEqualTo(Integer value) {
            addCriterion("sum_follower_number >=", value, "sumFollowerNumber");
            return (Criteria) this;
        }

        public Criteria andSumFollowerNumberLessThan(Integer value) {
            addCriterion("sum_follower_number <", value, "sumFollowerNumber");
            return (Criteria) this;
        }

        public Criteria andSumFollowerNumberLessThanOrEqualTo(Integer value) {
            addCriterion("sum_follower_number <=", value, "sumFollowerNumber");
            return (Criteria) this;
        }

        public Criteria andSumFollowerNumberIn(List<Integer> values) {
            addCriterion("sum_follower_number in", values, "sumFollowerNumber");
            return (Criteria) this;
        }

        public Criteria andSumFollowerNumberNotIn(List<Integer> values) {
            addCriterion("sum_follower_number not in", values, "sumFollowerNumber");
            return (Criteria) this;
        }

        public Criteria andSumFollowerNumberBetween(Integer value1, Integer value2) {
            addCriterion("sum_follower_number between", value1, value2, "sumFollowerNumber");
            return (Criteria) this;
        }

        public Criteria andSumFollowerNumberNotBetween(Integer value1, Integer value2) {
            addCriterion("sum_follower_number not between", value1, value2, "sumFollowerNumber");
            return (Criteria) this;
        }

        public Criteria andSumArticleNumberIsNull() {
            addCriterion("sum_article_number is null");
            return (Criteria) this;
        }

        public Criteria andSumArticleNumberIsNotNull() {
            addCriterion("sum_article_number is not null");
            return (Criteria) this;
        }

        public Criteria andSumArticleNumberEqualTo(Integer value) {
            addCriterion("sum_article_number =", value, "sumArticleNumber");
            return (Criteria) this;
        }

        public Criteria andSumArticleNumberNotEqualTo(Integer value) {
            addCriterion("sum_article_number <>", value, "sumArticleNumber");
            return (Criteria) this;
        }

        public Criteria andSumArticleNumberGreaterThan(Integer value) {
            addCriterion("sum_article_number >", value, "sumArticleNumber");
            return (Criteria) this;
        }

        public Criteria andSumArticleNumberGreaterThanOrEqualTo(Integer value) {
            addCriterion("sum_article_number >=", value, "sumArticleNumber");
            return (Criteria) this;
        }

        public Criteria andSumArticleNumberLessThan(Integer value) {
            addCriterion("sum_article_number <", value, "sumArticleNumber");
            return (Criteria) this;
        }

        public Criteria andSumArticleNumberLessThanOrEqualTo(Integer value) {
            addCriterion("sum_article_number <=", value, "sumArticleNumber");
            return (Criteria) this;
        }

        public Criteria andSumArticleNumberIn(List<Integer> values) {
            addCriterion("sum_article_number in", values, "sumArticleNumber");
            return (Criteria) this;
        }

        public Criteria andSumArticleNumberNotIn(List<Integer> values) {
            addCriterion("sum_article_number not in", values, "sumArticleNumber");
            return (Criteria) this;
        }

        public Criteria andSumArticleNumberBetween(Integer value1, Integer value2) {
            addCriterion("sum_article_number between", value1, value2, "sumArticleNumber");
            return (Criteria) this;
        }

        public Criteria andSumArticleNumberNotBetween(Integer value1, Integer value2) {
            addCriterion("sum_article_number not between", value1, value2, "sumArticleNumber");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}