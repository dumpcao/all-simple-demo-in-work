package com.talkyoung.ptbpojo.mapper;

import com.talkyoung.ptbpojo.entity.UserArticleBrowse;
import com.talkyoung.ptbpojo.entity.UserArticleBrowseExample;
import com.talkyoung.ptbpojo.entity.UserArticleBrowseKey;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface UserArticleBrowseMapper {
    int countByExample(UserArticleBrowseExample example);

    int deleteByExample(UserArticleBrowseExample example);

    int deleteByPrimaryKey(UserArticleBrowseKey key);

    int insert(UserArticleBrowse record);

    int insertSelective(UserArticleBrowse record);

    List<UserArticleBrowse> selectByExample(UserArticleBrowseExample example);

    UserArticleBrowse selectByPrimaryKey(UserArticleBrowseKey key);

    int updateByExampleSelective(@Param("record") UserArticleBrowse record, @Param("example") UserArticleBrowseExample example);

    int updateByExample(@Param("record") UserArticleBrowse record, @Param("example") UserArticleBrowseExample example);

    int updateByPrimaryKeySelective(UserArticleBrowse record);

    int updateByPrimaryKey(UserArticleBrowse record);
}