package com.talkyoung.ptbpojo.entity;

public class BlobArticleWithBLOBs extends BlobArticle {
    private String content;

    private String commonContent;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    public String getCommonContent() {
        return commonContent;
    }

    public void setCommonContent(String commonContent) {
        this.commonContent = commonContent == null ? null : commonContent.trim();
    }
}