package com.talkyoung.ptbpojo.entity;

public class UserArticleThumbUp extends UserArticleThumbUpKey {
    private String thumpUpTime;

    public String getThumpUpTime() {
        return thumpUpTime;
    }

    public void setThumpUpTime(String thumpUpTime) {
        this.thumpUpTime = thumpUpTime == null ? null : thumpUpTime.trim();
    }
}