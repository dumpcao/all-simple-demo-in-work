package com.talkyoung.ptbpojo.mapper;

import com.talkyoung.ptbpojo.entity.BlobArticle;
import com.talkyoung.ptbpojo.entity.BlobArticleExample;
import com.talkyoung.ptbpojo.entity.BlobArticleWithBLOBs;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface BlobArticleMapper {
    int countByExample(BlobArticleExample example);

    int deleteByExample(BlobArticleExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(BlobArticleWithBLOBs record);

    int insertSelective(BlobArticleWithBLOBs record);

    List<BlobArticleWithBLOBs> selectByExampleWithBLOBs(BlobArticleExample example);

    List<BlobArticle> selectByExample(BlobArticleExample example);

    BlobArticleWithBLOBs selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") BlobArticleWithBLOBs record, @Param("example") BlobArticleExample example);

    int updateByExampleWithBLOBs(@Param("record") BlobArticleWithBLOBs record, @Param("example") BlobArticleExample example);

    int updateByExample(@Param("record") BlobArticle record, @Param("example") BlobArticleExample example);

    int updateByPrimaryKeySelective(BlobArticleWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(BlobArticleWithBLOBs record);

    int updateByPrimaryKey(BlobArticle record);
}