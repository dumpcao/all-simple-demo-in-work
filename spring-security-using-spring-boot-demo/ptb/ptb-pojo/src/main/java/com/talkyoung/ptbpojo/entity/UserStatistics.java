package com.talkyoung.ptbpojo.entity;

public class UserStatistics {
    private Integer userId;

    private Integer sumViewTimes;

    private Integer sumCommentTimes;

    private Integer sumCollectTimes;

    private Integer sumThumbUpTimes;

    private Integer sumFansNumber;

    private Integer sumFollowerNumber;

    private Integer sumArticleNumber;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getSumViewTimes() {
        return sumViewTimes;
    }

    public void setSumViewTimes(Integer sumViewTimes) {
        this.sumViewTimes = sumViewTimes;
    }

    public Integer getSumCommentTimes() {
        return sumCommentTimes;
    }

    public void setSumCommentTimes(Integer sumCommentTimes) {
        this.sumCommentTimes = sumCommentTimes;
    }

    public Integer getSumCollectTimes() {
        return sumCollectTimes;
    }

    public void setSumCollectTimes(Integer sumCollectTimes) {
        this.sumCollectTimes = sumCollectTimes;
    }

    public Integer getSumThumbUpTimes() {
        return sumThumbUpTimes;
    }

    public void setSumThumbUpTimes(Integer sumThumbUpTimes) {
        this.sumThumbUpTimes = sumThumbUpTimes;
    }

    public Integer getSumFansNumber() {
        return sumFansNumber;
    }

    public void setSumFansNumber(Integer sumFansNumber) {
        this.sumFansNumber = sumFansNumber;
    }

    public Integer getSumFollowerNumber() {
        return sumFollowerNumber;
    }

    public void setSumFollowerNumber(Integer sumFollowerNumber) {
        this.sumFollowerNumber = sumFollowerNumber;
    }

    public Integer getSumArticleNumber() {
        return sumArticleNumber;
    }

    public void setSumArticleNumber(Integer sumArticleNumber) {
        this.sumArticleNumber = sumArticleNumber;
    }
}