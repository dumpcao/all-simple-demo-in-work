package com.talkyoung.ptbpojo.mapper;

import com.talkyoung.ptbpojo.entity.UserStatistics;
import com.talkyoung.ptbpojo.entity.UserStatisticsExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface UserStatisticsMapper {
    int countByExample(UserStatisticsExample example);

    int deleteByExample(UserStatisticsExample example);

    int deleteByPrimaryKey(Integer userId);

    int insert(UserStatistics record);

    int insertSelective(UserStatistics record);

    List<UserStatistics> selectByExample(UserStatisticsExample example);

    UserStatistics selectByPrimaryKey(Integer userId);

    int updateByExampleSelective(@Param("record") UserStatistics record, @Param("example") UserStatisticsExample example);

    int updateByExample(@Param("record") UserStatistics record, @Param("example") UserStatisticsExample example);

    int updateByPrimaryKeySelective(UserStatistics record);

    int updateByPrimaryKey(UserStatistics record);
}