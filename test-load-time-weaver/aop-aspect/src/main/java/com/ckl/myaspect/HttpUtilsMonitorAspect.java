package com.ckl.myaspect;

import com.alibaba.fastjson.JSON;
import com.ceiec.trace.filter.RequestHolder;
import com.ceiec.trace.task.LogReqRunable;
import com.ceiec.trace.utils.IpPortContextVO;
import com.ceiec.trace.utils.UrlParser;
import com.ceiec.trace.vo.CurrentRequestMetaData;
import com.ceiec.trace.vo.PeerToPeerRequestTrace;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.Map;

/**
 * 该切面已经在resources下的META-INF中进行配置，否则不会生效。
 *
 * desc: 监控httpUtils方法的发送post请求的方法，进行增强。
 * 1：在header中添加traceId，这样的话，下一个收到该请求的应用即可获取到traceId，方便进行全链路跟踪
 * 2: 构造请求vo，主要包含了该次请求的源主机、端口，目的主机、端口，请求参数，url，返回的响应，花费的时长等
 * 3: 用第二步的vo，发送到日志平台，平台对其存库。
 * @author: caokunliang
 * creat_date: 2018/10/25 0025
 * creat_time: 14:24
 **/
@Aspect
public class HttpUtilsMonitorAspect {
    private static final Logger logger = LoggerFactory.getLogger(HttpUtilsMonitorAspect.class);

    @Around("methodsToBeProfiled()")
    public Object profile(ProceedingJoinPoint pjp) throws Throwable {
        //获取参数
        Object[] args = pjp.getArgs();
        if (args == null || args.length != 3) {
            throw new RuntimeException("能进入该方法的不可能参数为空或者参数数量不为3");
        }

        //要拦截的方法的三个参数分别为url/headers/json表示的请求参数
        Map<String,String> headerMap = (Map<String, String>) args[1];
        CurrentRequestMetaData data = RequestHolder.get();
        headerMap.put("traceId",data.getTraceId());

        System.out.println("after: " + headerMap);


        logger.info("detect request:{}",args);
        Object result = pjp.proceed();



        return result;
    }



    @Pointcut("execution(public * com.ceiec.base.common.utilities.HttpUtils.doPostBigData(String, java.util.Map<String,String>, String))")
    public void methodsToBeProfiled(){}
}
