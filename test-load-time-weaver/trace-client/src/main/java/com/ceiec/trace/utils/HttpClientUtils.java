/******************************************************************
 * HttpUtils.java
 * Copyright 2017 by CEIEC Company. All Rights Reserved.
 * CreateDate：2017年5月11日
 * Author：李涛
 * Version：1.0.0
 ******************************************************************/

package com.ceiec.trace.utils;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.apache.http.Header;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


/**
 * <b>修改记录：</b>
 * <p>
 * <li>
 * <p>
 * ---- 李涛 2017年5月11日
 * </li>
 * </p>
 * <p>
 * <b>类说明：</b>
 * <p>
 * HTTP工具类
 * </p>
 */
public final class HttpClientUtils {


    private static int TIMEOUT = 30000;

    private static final String APPLICATION_JSON = "application/json";

    private static final int BUFFER_SIZE = 1024;

    /**
     * 编码格式
     */
    private static final String CODE_STYLE = "UTF-8";

    public static final String UTF8 = "UTF-8";

    private static Logger logger = LoggerFactory.getLogger(HttpClientUtils.class);


    private HttpClientUtils() {

    }


}
