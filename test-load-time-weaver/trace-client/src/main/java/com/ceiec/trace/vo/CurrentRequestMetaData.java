package com.ceiec.trace.vo;

import lombok.Data;

/**
 * desc:
 *
 * @author : caokunliang
 * creat_date: 2018/9/12 0012
 * creat_time: 11:44
 **/
@Data
public class CurrentRequestMetaData {
    private String traceId;

    private String srcHost;

    private String srcPort;

    private String destHost;

    private String destAppName;

    private String destPort;

    private String requestUrl;

}
