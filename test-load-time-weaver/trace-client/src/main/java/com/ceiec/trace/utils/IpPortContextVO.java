package com.ceiec.trace.utils;

import lombok.Data;

/**
 * desc:
 *
 * @author : caokunliang
 * creat_date: 2018/10/25 0025
 * creat_time: 11:45
 **/
@Data
public class IpPortContextVO {
    private String host;

    private String port;

    private String context;
}
