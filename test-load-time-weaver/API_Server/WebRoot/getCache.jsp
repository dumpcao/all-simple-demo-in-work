<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page contentType="text/html;charset=UTF-8"%>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <%@include file="/common/taglibs.jsp"%>
    <meta charset="UTF-8">
    <title>席位监控</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="keywords" content="席位监控,SCM">
    <meta http-equiv="description" content="席位监控,SCM">
    <!--
        <link rel="stylesheet" type="text/css" href="styles.css">
        -->
</head>

<body>
<p>
    席位监控(<a href="refresh.do">刷新</a>)--接处警席位缓存(<a href="javascript:void(0);" onclick="viewCAD();" target="_blank">查看</a>)
</p>
<table class="table table-striped table-bordered">
    <tr>
        <td width="18%">席位ID</td>
        <td width="10%">席位名</td>
        <td width="5">席位电话</td>
        <td width="18">用户ID</td>
        <td width="9%">用户名</td>
        <td width="10%">ip</td>
        <td width="10%">角色代码</td>
        <td width="5%">席位状态</td>
        <td width="10%">所属中心</td>
        <td width="5%">操作</td>
    </tr>
    <c:forEach items="${seats}" var="seat">
        <tr>
            <td>${seat.seatInformationId}</td>
            <td>${seat.seatInformationName}</td>
            <td>${seat.seatPhone}</td>
            <td>${seat.userId}</td>
            <td>${seat.userName}</td>
            <td>${seat.seatComputerIp}</td>
            <td>${seat.seatTypeId}
                <c:if test="${seat.seatTypeId==1}">
                    普通接警员
                </c:if>
                <c:if test="${seat.seatTypeId==11}">
                    警察类处警员
                </c:if>
                <c:if test="${seat.seatTypeId==12}">
                    消防类处警员
                </c:if>
                <c:if test="${seat.seatTypeId==13}">
                    交通类处警员
                </c:if>
                <c:if test="${seat.seatTypeId==14}">
                    医疗类处警员
                </c:if>
                <c:if test="${seat.seatTypeId==21}">
                    高级接警员
                </c:if>
                <c:if test="${seat.seatTypeId==31}">
                    高级处警员
                </c:if>
                <c:if test="${seat.seatTypeId==41}">
                    协调员
                </c:if>
                <c:if test="${seat.seatTypeId==51}">
                    值班主任
                </c:if>
                <c:if test="${seat.seatTypeId==71}">
                    接处警员席位
                </c:if>
                <c:if test="${seat.seatTypeId==81}">
                    值班长席位
                </c:if>
            </td>
            <td><c:if test="${seat.seatStatus eq '0'}">
                    空闲
                </c:if>
                <c:if test="${seat.seatStatus eq '1'}">
                    忙碌
                </c:if>
            </td>
            <td><c:if test="${fn:length(seat.centerId) == 2}">
                    (一级中心)
                </c:if>
                <c:if test="${fn:length(seat.centerId) == 4}">
                    (二级中心)
                </c:if>
                <c:if test="${fn:length(seat.centerId) == 6}">
                    (三级中心)
                </c:if>
                ${seat.centerName}
            </td>
            <td><a href="kickoff.do?seatId=${seat.seatInformationId}">断开</a></td>
        </tr>
    </c:forEach>
</table>
</body>
<script type="text/javascript">
    function viewCAD() {
        window.open("http://"+location.host.replace(":9080",":8080")+"/CAD_WebService/getCache.do");
    }
</script>
</html>
