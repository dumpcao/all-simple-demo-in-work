package com.example.webdemo.service.impl;

import com.example.webdemo.service.ITestBService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * desc:
 *
 * @author : ckl
 * creat_date: 2019/12/12 0012
 * creat_time: 8:48
 **/
@Service
@Slf4j
public class ITestBServiceImpl implements ITestBService {
    @Override
    public void doBusiness(String s) {
        log.info("ITestBServiceImpl doBusiness");
    }
}
