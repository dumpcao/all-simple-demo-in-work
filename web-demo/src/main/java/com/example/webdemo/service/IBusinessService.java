package com.example.webdemo.service;


/**
 * <p>
 * 处置分派记录 服务类
 * </p>
 *
 * @author mybatis_plus_generator
 * @since 2019-10-31
 */
public interface IBusinessService {

    void doBusiness(String s);
}
