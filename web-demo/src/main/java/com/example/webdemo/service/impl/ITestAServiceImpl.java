package com.example.webdemo.service.impl;

import com.example.webdemo.service.ITestAService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * desc:
 *
 * @author : ckl
 * creat_date: 2019/12/12 0012
 * creat_time: 8:38
 **/
@Service
@Slf4j
public class ITestAServiceImpl implements ITestAService {
    @Override
    public void doBusiness(String s) {
        log.info("doBusiness in ITestAServiceImpl:{}",s);
    }
}
