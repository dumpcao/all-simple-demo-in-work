package com.example.webdemo;

import com.example.webdemo.processor.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * desc:
 *
 * @author : ckl
 * creat_date: 2019/11/11 0011
 * creat_time: 15:46
 **/
@Component
public class InitRunner implements ApplicationListener<ContextRefreshedEvent> {
   private static final Logger log = LoggerFactory.getLogger(InitRunner.class);



    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        ApplicationContext applicationContext = event.getApplicationContext();
        log.info("onApplicationEvent:event:{},applicationcontextType:{},name:{}",event.getClass(), applicationContext.getClass(),applicationContext.getDisplayName());
    }
}
