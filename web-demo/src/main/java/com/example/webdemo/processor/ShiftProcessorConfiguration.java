package com.example.webdemo.processor;

import com.example.webdemo.service.ITestAService;
import com.example.webdemo.service.ITestBService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * desc:
 *
 * @author : ckl
 * creat_date: 2019/12/9 0009
 * creat_time: 19:34
 **/
@Configuration
@Slf4j
public class ShiftProcessorConfiguration {

    @Bean
    public Processor testProcessA(ITestAService iTestAService){
        Processor processor = new Processor<String>() {
            @Override
            public Boolean supports(String s) {
                if ("A".equalsIgnoreCase(s)) {
                    return true;
                }
                return false;
            }

            @Override
            public void process(String s) {
                log.info("process A is processing:{}",s);
                iTestAService.doBusiness(s);
            }


        };
        return processor;
    }


    @Bean
    public Processor testProcessB(ITestBService iTestBService){
        Processor processor = new Processor<String>() {
            @Override
            public Boolean supports(String s) {
                if ("B".equalsIgnoreCase(s)) {
                    return true;
                }
                return false;
            }

            @Override
            public void process(String s) {
                log.info("process B is processing:{}",s);
                iTestBService.doBusiness(s);
            }


        };
        return processor;
    }




}
