package com.example.webdemo.processor;



/**
 * desc:
 *
 * @author : ckl
 * creat_date: 2019/12/9 0009
 * creat_time: 19:24
 **/
public  interface Processor<T> {

    /**
     * 是否支持处理本类
     * @param t
     * @return
     */
    Boolean supports(T t);


    /**
     * 进行真正的业务处理的地方；是否需要返回值，可自己考虑
     * @param t
     */
    void process(T t);
}
