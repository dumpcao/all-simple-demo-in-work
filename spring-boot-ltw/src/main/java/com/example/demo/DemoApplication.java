package com.example.demo;

import org.apache.catalina.connector.CoyoteAdapter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableLoadTimeWeaving;

@SpringBootApplication
//@EnableLoadTimeWeaving
public class DemoApplication {

	public static void main(String[] args) {
		System.setProperty("org.aspectj.weaver.showWeaveInfo", "true");
		System.setProperty("aj.weaving.verbose", "true");
		SpringApplication.run(DemoApplication.class, args);
		ClassLoader classLoader = CoyoteAdapter.class.getClassLoader();
		System.out.println(classLoader);
	}

}
