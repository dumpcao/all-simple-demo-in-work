package com.example.demo;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;

/**
 * Create by chenjian on 2020/3/11.
 */
@Aspect
@Slf4j
public class RequestSessionIdAspect {

//        @Pointcut("execution(* org.apache.catalina.connector.CoyoteAdapter.parseSessionCookiesId(org.apache.catalina.connector.Request))")
//    @Pointcut("execution(* org.springframework.web.servlet.DispatcherServlet.*(*))")
    @Pointcut("execution(public * org.springframework.web.servlet.DispatcherServlet.*(..))")
//    @Pointcut("execution(public * com.example.demo..*.*(..))")
    public void methodsToBeProfiled() {
    }

    @Around("methodsToBeProfiled()")
    public Object around(ProceedingJoinPoint point) throws Throwable {
        //Request request = (Request) point.getArgs()[0];
        //request.setRequestedSessionId(request.getHeader("Authorization"));
        log.info("ahhsahhh");
        return point.proceed();
    }
}
