package com.example.webdemo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.webdemo.model.AppealDisposalOnOff;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author mybatis_plus_generator
 * @since 2019-11-15
 */
public interface IAppealDisposalOnOffService extends IService<AppealDisposalOnOff> {

    List<AppealDisposalOnOff> getPersonListNotAcceptIncidentDisposal();



}
