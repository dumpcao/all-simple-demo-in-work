package com.example.webdemo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.webdemo.mapper.AppealDisposalOnOffMapper;
import com.example.webdemo.model.AppealDisposalOnOff;
import com.example.webdemo.service.IAppealDisposalOnOffService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
@Slf4j
public class AppealDisposalOnOffServiceImpl extends ServiceImpl<AppealDisposalOnOffMapper, AppealDisposalOnOff> implements IAppealDisposalOnOffService {


    @Override
    public List<AppealDisposalOnOff> getPersonListNotAcceptIncidentDisposal() {
        LambdaQueryWrapper<AppealDisposalOnOff> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(AppealDisposalOnOff::getDisposalOnOffStatus,AppealDisposalOnOff.ACCEPT_OFF);
        List<AppealDisposalOnOff> list = this.list(wrapper);
        return list;
    }


}
