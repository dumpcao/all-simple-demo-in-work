package com.example.webdemo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.webdemo.model.AppealDisposalOnOff;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author mybatis_plus_generator
 * @since 2019-11-15
 */
public interface AppealDisposalOnOffMapper extends BaseMapper<AppealDisposalOnOff> {

}
