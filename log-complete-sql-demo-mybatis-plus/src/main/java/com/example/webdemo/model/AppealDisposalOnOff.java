package com.example.webdemo.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.experimental.Accessors;


@Data
@Accessors(chain = true)
public class AppealDisposalOnOff {

    /**
     * 打开
     */
    public static final int ACCEPT_ON = 1;

    /**
     * 关闭
     */
    public static final int ACCEPT_OFF = 0;

    @TableId(value = "appeal_disposal_on_off_id", type = IdType.ID_WORKER)
    private Long appealDisposalOnOffId;

    /**
     * 用户id
     */
    private Long userId;

    private Integer appealOnOffStatus;

    private Integer disposalOnOffStatus;
}
