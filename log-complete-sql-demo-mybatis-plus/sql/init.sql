/*
SQLyog v10.2 
MySQL - 5.7.25 : Database - cad
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`test` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */;

USE `test`;

/*Table structure for table `appeal_disposal_on_off` */

DROP TABLE IF EXISTS `appeal_disposal_on_off`;

CREATE TABLE `appeal_disposal_on_off` (
  `appeal_disposal_on_off_id` bigint(20) NOT NULL,
  `appeal_on_off_status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '开关状态',
  `disposal_on_off_status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '开关状态',
  `user_id` bigint(20) NOT NULL COMMENT '用户id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `appeal_disposal_on_off` */

insert  into `appeal_disposal_on_off`(`appeal_disposal_on_off_id`,`appeal_on_off_status`,`disposal_on_off_status`,`user_id`) values (1111,1,1,12),(1211471734998573057,0,0,1200256226441940994),(1211570244791881729,0,0,1200395585908240385),(1211573119907581953,0,0,1200398441965023233),(1211573274381467649,0,1,1207930042154037249),(1211574979229962241,0,0,1205041586367389698),(1211582634128363521,0,0,1202495436825309185),(1211588206412197889,0,0,1200081698335870978),(1211821727164665857,0,0,1207140667709497345);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
