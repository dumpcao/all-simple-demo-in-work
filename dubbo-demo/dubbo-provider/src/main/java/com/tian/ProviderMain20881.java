package com.tian;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;

/**
 * Hello world!
 * @author lawt
 */
public class ProviderMain20881 {
    public static void main(String[] args) throws IOException {
        System.out.println("dubbo服务provider启动成功");
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("provider-20881.xml");
        ctx.start();
        System.out.println("dubbo服务provider启动成功");
        System.in.read();
    }
}
