package com.tian;

import com.tian.service.UserService;
import io.netty.channel.ChannelFuture;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Scanner;

/**
 * 消费端
 * @author lawt
 */
public class ConsumerMain {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("consumer.xml");
        UserService sayHello = (UserService) ctx.getBean(UserService.class);
        Scanner sca = new Scanner(System.in);

        while (true) {
            String str = sca.nextLine();
            if (str.equals("exit")) {
                break;
            }
            String s = sayHello.getUserById(1111);
            System.out.println(s);
        }

    }
}
