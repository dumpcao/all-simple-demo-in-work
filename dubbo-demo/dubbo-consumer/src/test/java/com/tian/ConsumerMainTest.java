package com.tian;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * Unit test for simple ConsumerMain.
 */
public class ConsumerMainTest
{
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        int a = 2147483647;
        System.out.println(a + 1);
        long l = System.currentTimeMillis();
        CompletableFuture<String> future = new CompletableFuture<>();
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep((long) (Math.random() * 2000));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName());
                boolean b = future.complete("hahha");
//                boolean b = future.completeExceptionally(new RuntimeException("ajsk"));
                System.out.println(b);
            }
        });
        thread.start();


        String s = future.get();
        long spent = System.currentTimeMillis() - l;
        System.out.println("get result:" +  s + ",spent " + spent + " ms");

    }
}
