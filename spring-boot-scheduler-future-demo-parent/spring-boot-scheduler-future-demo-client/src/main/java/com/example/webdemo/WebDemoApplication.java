package com.example.webdemo;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@SpringBootApplication
@Slf4j
public class WebDemoApplication{

	public static void main(String[] args) {
        SpringApplication springApplication = new SpringApplication(WebDemoApplication.class);
        springApplication.setWebApplicationType(WebApplicationType.NONE);
        springApplication.run();
    }

    @Bean
    public RestTemplate restTemplate() {
	    return new RestTemplate();
    }
}
