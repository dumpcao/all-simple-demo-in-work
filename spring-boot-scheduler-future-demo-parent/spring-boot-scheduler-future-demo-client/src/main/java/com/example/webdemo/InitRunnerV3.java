package com.example.webdemo;

import com.example.webdemo.webdemo1.CustomFutureCallBack;
import com.example.webdemo.webdemo1.CustomScheduledFuture;
import com.example.webdemo.webdemo1.CustomScheduledThreadPoolExecutor;
import com.example.webdemo.webdemo1.TestTaskV3;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * desc:
 *
 * @author : ckl
 * creat_date: 2019/11/11 0011
 * creat_time: 15:46
 **/
@Component
@Slf4j
public class InitRunnerV3 implements CommandLineRunner {

    @Autowired
    private RestTemplate restTemplate;

    CustomScheduledThreadPoolExecutor scheduledThreadPoolExecutor =
            new CustomScheduledThreadPoolExecutor(1, new NamedThreadFactory("init-data-from-third-sys"));

    @Override
    public void run(String... args)  {
        TestTaskV3 task = new TestTaskV3(restTemplate);
        CustomScheduledFuture<?> scheduledFuture = scheduledThreadPoolExecutor.scheduleAtFixedRate(task,
                        0, 10, TimeUnit.SECONDS);
        scheduledFuture.setCustomFutureCallBack(new CustomFutureCallBack() {

            @Override
            public void onSuccess(CustomScheduledFuture customScheduledFuture) {
                log.info("onSuccess");
                customScheduledFuture.cancel(true);
            }

            @Override
            public void onException(Throwable throwable) {
                log.error("e:{}",throwable);
            }
        });
    }


}
