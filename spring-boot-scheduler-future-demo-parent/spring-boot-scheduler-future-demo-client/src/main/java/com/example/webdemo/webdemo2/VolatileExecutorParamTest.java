package com.example.webdemo.webdemo2;

import com.example.webdemo.NamedThreadFactory;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;

@Slf4j
public class VolatileExecutorParamTest {
    public static void main(String[] args) throws InterruptedException {
        NamedThreadFactory factory = new NamedThreadFactory("VolatileExecutorParamTest");
        ThreadPoolExecutor executor = new ThreadPoolExecutor(1, 1, 20, TimeUnit.SECONDS,
                new LinkedBlockingQueue<>(2000), factory,new
                ThreadPoolExecutor.DiscardPolicy());
        for (int i = 0; i < 1000020; i++) {
            int finalI = i;
            executor.submit(new Runnable() {
                @Override
                public void run() {
                    try {
                        TimeUnit.MILLISECONDS.sleep(10000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    log.info(":{}", finalI);
                }
            });
        }

        while (true) {
            log.info("core size:{}",executor.getCorePoolSize());
            Thread.sleep(50);
            executor.setCorePoolSize(executor.getCorePoolSize() + 1);
            if (executor.getCorePoolSize() > 50) {
                log.info("arrvie 50");
                break;
            }
        }
        LockSupport.park();
    }
}
