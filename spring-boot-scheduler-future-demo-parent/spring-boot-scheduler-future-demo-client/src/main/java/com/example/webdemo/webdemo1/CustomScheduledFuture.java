package com.example.webdemo.webdemo1;

import lombok.Data;

import java.util.concurrent.*;

@Data
public class CustomScheduledFuture<V> implements RunnableScheduledFuture<V> {
    /**
     * 其实是下面这种类型：
     * {@link java.util.concurrent.ScheduledThreadPoolExecutor.ScheduledFutureTask
     *
     */
    RunnableScheduledFuture<V> scheduledFuture;

    CustomFutureCallBack customFutureCallBack;

    Runnable runnable;


    @Override
    public long getDelay(TimeUnit unit) {
        return scheduledFuture.getDelay(unit);
    }

    @Override
    public int compareTo(Delayed o) {
        return scheduledFuture.compareTo(o);
    }

    @Override
    public boolean cancel(boolean mayInterruptIfRunning) {
        return scheduledFuture.cancel(mayInterruptIfRunning);
    }

    @Override
    public boolean isCancelled() {
        return scheduledFuture.isCancelled();
    }

    @Override
    public boolean isDone() {
        return scheduledFuture.isDone();
    }

    @Override
    public V get() throws InterruptedException, ExecutionException {
        return scheduledFuture.get();
    }

    @Override
    public V get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
        return scheduledFuture.get(timeout, unit);
    }

    @Override
    public boolean isPeriodic() {
        return scheduledFuture.isPeriodic();
    }

    @Override
    public void run() {
        scheduledFuture.run();
    }
}
