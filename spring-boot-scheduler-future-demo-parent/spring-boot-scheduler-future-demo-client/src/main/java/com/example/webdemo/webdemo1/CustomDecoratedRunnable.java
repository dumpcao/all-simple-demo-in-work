package com.example.webdemo.webdemo1;

import lombok.Data;

@Data
public class CustomDecoratedRunnable implements Runnable {
    Runnable runnable;

    CustomScheduledFuture customScheduledFuture;



    public CustomDecoratedRunnable(Runnable runnable,CustomScheduledFuture customScheduledFuture) {
        this.runnable = runnable;
        this.customScheduledFuture = customScheduledFuture;
    }

    @Override
    public void run() {
        this.runnable.run();
    }


}
