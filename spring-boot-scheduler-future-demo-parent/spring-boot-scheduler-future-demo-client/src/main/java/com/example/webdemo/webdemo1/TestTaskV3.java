package com.example.webdemo.webdemo1;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.ScheduledFuture;

@Slf4j
public class TestTaskV3 implements Runnable{
    private RestTemplate restTemplate;


    public TestTaskV3(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public void run() {
        try {
            ResponseEntity<String> entity = restTemplate.getForEntity("http://localhost:8082", String.class);
            String s = entity.toString();
            log.info("get data:{}",s);
        } catch (Exception e) {
//            log.error("e:{}",e);
            log.error("error");
            throw e;
        }

    }

}
