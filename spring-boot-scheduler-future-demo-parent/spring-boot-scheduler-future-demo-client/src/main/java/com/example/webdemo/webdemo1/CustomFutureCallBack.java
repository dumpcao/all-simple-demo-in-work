package com.example.webdemo.webdemo1;

public interface CustomFutureCallBack {

    void onSuccess(CustomScheduledFuture customScheduledFuture);


    void onException(Throwable throwable);
}
