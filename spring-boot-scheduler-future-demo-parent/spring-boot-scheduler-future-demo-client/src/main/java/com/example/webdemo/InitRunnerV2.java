package com.example.webdemo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * desc:
 *
 * @author : ckl
 * creat_date: 2019/11/11 0011
 * creat_time: 15:46
 **/
//@Component
public class InitRunnerV2 implements CommandLineRunner {

    @Autowired
    private RestTemplate restTemplate;

    ScheduledThreadPoolExecutor scheduledThreadPoolExecutor =
            new ScheduledThreadPoolExecutor(1, new NamedThreadFactory("init-data-from-third-sys"));

    @Override
    public void run(String... args)  {
        TestTask task = new TestTask(restTemplate);
        ScheduledFuture<?> scheduledFuture = scheduledThreadPoolExecutor.scheduleAtFixedRate(task,
                        0, 10, TimeUnit.SECONDS);
        task.setScheduledFuture(scheduledFuture);
    }


}
