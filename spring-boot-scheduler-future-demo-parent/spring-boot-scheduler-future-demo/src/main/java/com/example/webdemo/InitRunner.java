package com.example.webdemo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * desc:
 *
 * @author : ckl
 * creat_date: 2019/11/11 0011
 * creat_time: 15:46
 **/
@Component
public class InitRunner implements  CommandLineRunner{
   private static final Logger log = LoggerFactory.getLogger(InitRunner.class);

    @Autowired
    private Environment environment;


    @Override
    public void run(String... args) throws Exception {
        log.info("测试方法：post方式访问url：http://localhost:{}/test.do",environment.getProperty("server.port"));

    }
}
