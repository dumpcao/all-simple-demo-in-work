package com.example.webdemo;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * desc:
 *
 * @author : ckl
 * creat_date: 2019/12/12 0012
 * creat_time: 8:51
 **/
@RestController
@Slf4j
public class BusinessController {


    @GetMapping("/")
    public String test() {
        return "success";
    }
}
