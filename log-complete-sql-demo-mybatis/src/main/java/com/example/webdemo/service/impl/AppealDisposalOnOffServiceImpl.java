package com.example.webdemo.service.impl;

import com.example.webdemo.mapper.AppealDisposalOnOffMapper;
import com.example.webdemo.model.AppealDisposalOnOff;
import com.example.webdemo.service.IAppealDisposalOnOffService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@Slf4j
public class AppealDisposalOnOffServiceImpl  implements IAppealDisposalOnOffService {
    @Autowired
    private AppealDisposalOnOffMapper appealDisposalOnOffMapper;

    @Override
    public List<AppealDisposalOnOff> getPersonListNotAcceptIncidentDisposal() {
        List<AppealDisposalOnOff> appealDisposalOnOffs = appealDisposalOnOffMapper.selectByDisposalOnOffStatus(AppealDisposalOnOff.ACCEPT_OFF);
        return appealDisposalOnOffs;
    }


}
