package com.example.webdemo.model;

import lombok.Data;
import lombok.experimental.Accessors;


@Data
public class AppealDisposalOnOff {

    /**
     * 打开
     */
    public static final int ACCEPT_ON = 1;

    /**
     * 关闭
     */
    public static final int ACCEPT_OFF = 0;

    private Long appealDisposalOnOffId;

    /**
     * 用户id
     */
    private Long userId;

    private Integer appealOnOffStatus;

    private Integer disposalOnOffStatus;
}
