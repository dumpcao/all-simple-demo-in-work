package com.example.webdemo.mapper;

import com.example.webdemo.model.AppealDisposalOnOff;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author mybatis_plus_generator
 * @since 2019-11-15
 */
public interface AppealDisposalOnOffMapper {

    public List<AppealDisposalOnOff> selectByDisposalOnOffStatus(@Param("disposalOnOffStatus") Integer disposalOnOffStatus);
}
