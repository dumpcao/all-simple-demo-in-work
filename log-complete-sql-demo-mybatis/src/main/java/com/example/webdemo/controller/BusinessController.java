package com.example.webdemo.controller;

import com.example.webdemo.model.AppealDisposalOnOff;
import com.example.webdemo.service.IAppealDisposalOnOffService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * desc:
 *
 * @author : ckl
 * creat_date: 2019/12/12 0012
 * creat_time: 8:51
 **/
@RestController
@Slf4j
public class BusinessController {
    @Autowired
    private IAppealDisposalOnOffService iAppealDisposalOnOffService;

    @GetMapping("test.do")
    public List<AppealDisposalOnOff> test(){
        return iAppealDisposalOnOffService.getPersonListNotAcceptIncidentDisposal();
    }


}
