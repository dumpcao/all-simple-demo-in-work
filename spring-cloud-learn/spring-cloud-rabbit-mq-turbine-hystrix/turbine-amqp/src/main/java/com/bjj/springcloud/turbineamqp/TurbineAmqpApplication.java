package com.bjj.springcloud.turbineamqp;
//import com.netflix.hystrix.contrib.metrics.eventstream.HystrixMetricsStreamServlet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.turbine.stream.EnableTurbineStream;

/**
 * @EnableTurbineStream: 开启EnableTurbineStream功能
 */
@EnableTurbineStream
@SpringBootApplication
public class TurbineAmqpApplication {
	public static void main(String[] args) {
		SpringApplication.run(TurbineAmqpApplication.class, args);
	}

}