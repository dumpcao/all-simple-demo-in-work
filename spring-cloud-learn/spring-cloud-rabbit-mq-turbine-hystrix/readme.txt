本例子的示例代码来自一位朋友，稍作了些整理和优化。
主要作用是实现以下架构：

消费者请求服务提供者时，是使用了hystrix。
消费者中的hystrix监控数据，会自己发送到rabbitmq的hystrixStreamOutput这个交换机里面。

然后turbine-amqp这个模块，是作为一个聚合者，从mq里的hystrixStreamOutput交换机，获取监控数据。

然后hystrix-dashboard负责监视turbine-amqp这个应用，用来展示相关数据。其中，turbine-amqp提供了对外接口，应该是和浏览器建立了长连接
，会主动推送数据到浏览器。

参考：
https://cloud.spring.io/spring-cloud-static/spring-cloud-netflix/2.0.4.RELEASE/multi/multi__hystrix_timeouts_and_ribbon_clients.html#_turbine_stream

这其中，消费者发给mq的内容大致如下，和直接通过访问消费者的hystrix.stream接口拿到的数据，大同小异（在amqp中发送的数据为json格式）：
{
  "origin": {
    "host": "USER-20160930CJ",
    "port": 9000,
    "serviceId": "ribbon-consumer",
    "id": "application-1"
  },
  "event": "message",
  "data": {
    "type": "HystrixCommand",
    "name": "ribbon-consumer.helloService",
    "group": "HelloService",
    "currentTime": 1582378017425,
    "isCircuitBreakerOpen": false,
    "errorPercentage": 0,
    "errorCount": 0,
    "requestCount": 0,
    "rollingCountCollapsedRequests": 0,
    "rollingCountExceptionsThrown": 0,
    "rollingCountFailure": 0,
    "rollingCountFallbackFailure": 0,
    "rollingCountFallbackRejection": 0,
    "rollingCountFallbackSuccess": 0,
    "rollingCountResponsesFromCache": 0,
    "rollingCountSemaphoreRejected": 0,
    "rollingCountShortCircuited": 0,
    "rollingCountSuccess": 0,
    "rollingCountThreadPoolRejected": 0,
    "rollingCountTimeout": 0,
    "currentConcurrentExecutionCount": 0,
    "latencyExecute_mean": 0,
    "latencyExecute": {
      "0": 0,
      "25": 0,
      "50": 0,
      "75": 0,
      "90": 0,
      "95": 0,
      "99": 0,
      "100": 0,
      "99.5": 0
    },
    "latencyTotal_mean": 0,
    "latencyTotal": {
      "0": 0,
      "25": 0,
      "50": 0,
      "75": 0,
      "90": 0,
      "95": 0,
      "99": 0,
      "100": 0,
      "99.5": 0
    },
    "propertyValue_circuitBreakerRequestVolumeThreshold": 1,
    "propertyValue_circuitBreakerSleepWindowInMilliseconds": 5000,
    "propertyValue_circuitBreakerErrorThresholdPercentage": 50,
    "propertyValue_circuitBreakerForceOpen": false,
    "propertyValue_circuitBreakerForceClosed": false,
    "propertyValue_circuitBreakerEnabled": true,
    "propertyValue_executionIsolationStrategy": "THREAD",
    "propertyValue_executionIsolationThreadTimeoutInMilliseconds": 1000,
    "propertyValue_executionIsolationThreadInterruptOnTimeout": true,
    "propertyValue_executionIsolationThreadPoolKeyOverride": null,
    "propertyValue_executionIsolationSemaphoreMaxConcurrentRequests": 10,
    "propertyValue_fallbackIsolationSemaphoreMaxConcurrentRequests": 10,
    "propertyValue_metricsRollingStatisticalWindowInMilliseconds": 10000,
    "propertyValue_requestCacheEnabled": true,
    "propertyValue_requestLogEnabled": true,
    "reportingHosts": 1
  }
}