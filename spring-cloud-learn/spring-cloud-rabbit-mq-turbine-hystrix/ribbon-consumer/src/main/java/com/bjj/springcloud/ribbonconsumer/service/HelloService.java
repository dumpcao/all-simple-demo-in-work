package com.bjj.springcloud.ribbonconsumer.service;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@Slf4j
public class HelloService {
    @Autowired
    RestTemplate restTemplate;

    @HystrixCommand(fallbackMethod = "helloFallBack")
    public String helloService(){
        long start = System.currentTimeMillis();
        String s = restTemplate.getForEntity("http://app-provider/provider", String.class).getBody();

        long end = System.currentTimeMillis();
        log.info("Spend Time:" +(end-start));

        return s;
    }

    public String helloFallBack(){
        return "此为hystrix托底数据，您被降级了";
    }
}
