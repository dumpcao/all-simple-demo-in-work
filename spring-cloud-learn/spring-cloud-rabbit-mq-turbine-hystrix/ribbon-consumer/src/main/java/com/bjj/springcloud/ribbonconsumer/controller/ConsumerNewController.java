package com.bjj.springcloud.ribbonconsumer.controller;

import com.bjj.springcloud.ribbonconsumer.service.HelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
@RestController
public class ConsumerNewController {

    @Autowired
    HelloService helloService;

    @RequestMapping(value = "/ribbon-consumer-new",method = RequestMethod.GET)
    public  String helloConsumer(){
        return  helloService.helloService();
    }
}
