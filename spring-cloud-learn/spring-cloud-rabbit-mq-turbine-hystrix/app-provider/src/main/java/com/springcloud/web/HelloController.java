package com.springcloud.web;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;

@RestController
@Slf4j
public class HelloController {

    @RequestMapping("/provider")
    public String provider() throws InterruptedException {
        //让线程等待三秒
        int sleepTime = new Random().nextInt(3000);
        Thread.sleep(sleepTime);
        log.info("sleepTime:{}",sleepTime);
        return "Hello,Provider!";
    }

}
