package com.example.webdemo.service.impl;

import com.example.webdemo.service.IBusinessService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * desc:
 *
 * @author : ckl
 * creat_date: 2019/12/12 0012
 * creat_time: 8:52
 **/
@Service
@Slf4j
public class IBusinessServiceImpl implements IBusinessService {

    @Override
    public void doBusiness(String s) {
        log.info("logstash appender send to logstash server " );
    }
}
