package com.example.webdemo;

import com.example.webdemo.service.IBusinessService;
import com.example.webdemo.dto.QueryDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * desc:
 *
 * @author : ckl
 * creat_date: 2019/12/12 0012
 * creat_time: 8:51
 **/
@RestController
@Slf4j
public class BusinessController {

    @Autowired
    private IBusinessService iBusinessService;

    @PostMapping("test.do")
    public void test(@Validated @RequestBody QueryDto queryDto){
        iBusinessService.doBusiness("A");
    }
}
