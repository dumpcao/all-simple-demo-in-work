package com.example.webdemo.annotation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * desc:
 *
 * @author : ckl
 * creat_date: 2019/12/12 0012
 * creat_time: 15:55
 **/
public class SpecialCharValidator implements ConstraintValidator<SpecialCharNotAllowed, Object> {

    @Override
    public boolean isValid(Object object, ConstraintValidatorContext constraintValidatorContext) {
        if (object == null) {
            return true;
        }
        if (object instanceof String) {
            String str = (String) object;
            if (str.contains("%")) {
                return false;
            }
        }
        return true;
    }
}
