package com.example.webdemo.annotation;

import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 注解，主要验证是否有特殊字符
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface SpecialCharNotAllowed {
//    String message() default "{javax.validation.constraints.Min.message}";
    String message() default "special char like '%' is illegal";

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };

}
