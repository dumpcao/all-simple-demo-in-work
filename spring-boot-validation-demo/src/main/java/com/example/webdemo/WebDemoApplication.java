package com.example.webdemo;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;

import java.util.List;

@SpringBootApplication
@Slf4j
public class WebDemoApplication{

	public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(WebDemoApplication.class, args);
    }

}
