package com.example.webdemo.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * desc:
 *
 * @author : ckl
 * creat_date: 2019/12/13 0013
 * creat_time: 18:40
 **/
@Data
public class QueryDto {
    /**
     * 查询关键字
     */
    @NotBlank
    private String keyword;

}
