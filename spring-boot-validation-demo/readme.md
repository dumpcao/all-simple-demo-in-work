spring boot应用使用validation api进行参数校验。
使用方式大概如下：
> 在接收请求的dto中，字段上加上validation包下的注解，比如下面这样
```java
@Data
public class QueryDto {
    /**
     * 查询关键字
     */
    @NotBlank
    private String keyword;

}
```
再在controller的endpoint上加上validated注解，如下：
```java
    @PostMapping("test.do")
    public void test(@Validated @RequestBody QueryDto queryDto){
        iBusinessService.doBusiness("A");
    }
```

即可对参数进行校验。