package com.ceiec.cad.vo;

import lombok.Data;

@Data
public class SingleGroupTextMsg {
    private String msg;

    private Integer type;

    private String to_wxid;

    private String robot_wxid;

}
