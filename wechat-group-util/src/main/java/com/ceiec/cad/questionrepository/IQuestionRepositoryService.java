package com.ceiec.cad.questionrepository;

public interface IQuestionRepositoryService {
    /**
     * 获取一个问题
     * @return
     */
    String getOneQuestion();
}
