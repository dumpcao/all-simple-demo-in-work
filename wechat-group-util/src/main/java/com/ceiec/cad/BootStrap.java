package com.ceiec.cad;

import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Hello world!
 */
@SpringBootApplication
@ComponentScan("com.ceiec")
@EnableScheduling
public class BootStrap {

    public static void main(String[] args) {
        new SpringApplicationBuilder(BootStrap.class).web(WebApplicationType.NONE).run(args);
    }
    
}
