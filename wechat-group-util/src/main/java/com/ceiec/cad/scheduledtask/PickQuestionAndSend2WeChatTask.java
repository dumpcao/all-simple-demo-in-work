package com.ceiec.cad.scheduledtask;

import com.alibaba.fastjson.JSONObject;
import com.ceiec.cad.questionrepository.QuestionRepository;
import com.ceiec.cad.util.HttpClientUtils;
import com.ceiec.cad.vo.SingleGroupTextMsg;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * 挑选问题，发送到微信群
 */
@Component
@Slf4j
public class PickQuestionAndSend2WeChatTask {

    @Autowired
    private QuestionRepository questionRepository;

    public static final String url = "http://10.15.4.46:8073/send";


    @Scheduled(cron = "${cron1}")
    @Scheduled(cron = "${cron2}")
    public void sendQuestionToWechatGroup() {
        String oneQuestion = questionRepository.getOneQuestion();
        SingleGroupTextMsg msg = new SingleGroupTextMsg();
        String temp = "今日问题:\n" + oneQuestion;
        try {
            msg.setMsg(URLEncoder.encode(temp, "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            log.error("e:{}", e);
            return;
        }
        msg.setType(100);
        msg.setTo_wxid("23161441516@chatroom");
        msg.setRobot_wxid("qiyepretty");

        String s = HttpClientUtils.doPost(url, JSONObject.toJSONString(msg));
        log.info("result:{}",s);
    }



    public static void main(String[] args) throws IOException {
        PickQuestionAndSend2WeChatTask task = new PickQuestionAndSend2WeChatTask();
        task.questionRepository = new QuestionRepository();
        task.sendQuestionToWechatGroup();
    }
}
