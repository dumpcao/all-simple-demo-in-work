package com.ceiec.cad.util;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class DateUtils {

    public static String getDateStr(DateTime date) {
        return date.toString("yyyy-MM-dd");
    }

    public static String getToday() {
        return getDateStr(DateTime.now());
    }
    public static String getTomorrow() {
        return getDateStr(DateTime.now().plusDays(1));
    }

    public static void main(String[] args) {
        isRest("2020-03-30");
        String today = getToday();
//        System.out.println(today);

        String tomorrow = getTomorrow();
//        System.out.println(tomorrow);
    }

    public static boolean isRest(String date) {
        DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd").withZone(DateTimeZone.forOffsetHours(8));
        //2019-10-28T10:23:12.000+06:00
        DateTime dateTime = fmt.parseDateTime(date);
//        System.out.println(dateTime);
        DateTime.Property day = dateTime.dayOfWeek();
        int i = day.get();
        return i == 6 || i == 7;
    }
}
