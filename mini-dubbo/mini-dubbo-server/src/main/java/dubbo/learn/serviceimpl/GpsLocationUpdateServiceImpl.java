package dubbo.learn.serviceimpl;

import dubbo.learn.CarCurrentLocationVO;
import dubbo.learn.IGpsLocationUpdateService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class GpsLocationUpdateServiceImpl implements IGpsLocationUpdateService {

    @Override
    public String refreshLocation(CarCurrentLocationVO currentLocationVO) {
        log.info("receive req:{}",currentLocationVO);
        return "success";
    }

}
