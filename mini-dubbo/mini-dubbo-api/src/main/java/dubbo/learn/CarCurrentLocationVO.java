package dubbo.learn;

import lombok.Data;

@Data
public class CarCurrentLocationVO {
    /**
     * 车牌号
     */
    private String carPlate;

    /**
     * 方向
     */
    private int direction;

    /**
     * 速度
     */
    private int speed;

}
