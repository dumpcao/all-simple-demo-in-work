/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dubbo.learn.core.config;

import dubbo.learn.core.config.parser.ExportServiceBeanDefinitionParser;
import dubbo.learn.core.config.parser.ProtocolBeanDefinitionParser;
import dubbo.learn.core.config.parser.RegistryBeanDefinitionParser;
import dubbo.learn.core.config.parser.ServiceReferenceBeanDefinitionParser;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.xml.NamespaceHandlerSupport;
import org.springframework.beans.factory.xml.ParserContext;
import org.w3c.dom.Element;

import static org.springframework.context.annotation.AnnotationConfigUtils.registerAnnotationConfigProcessors;

/**
 * DubboNamespaceHandler
 *
 * @export
 */
public class MiniDubboNamespaceHandler extends NamespaceHandlerSupport {

    @Override
    public void init() {
        registerBeanDefinitionParser("registry", new RegistryBeanDefinitionParser());
        /**
         * 针对客户端的配置
         */
        registerBeanDefinitionParser("reference", new ServiceReferenceBeanDefinitionParser());
        /**
         * 针对服务端要导出的服务的配置
         */
        registerBeanDefinitionParser("service", new ExportServiceBeanDefinitionParser());

        /**
         * 针对服务端的，协议的配置
         */
        registerBeanDefinitionParser("protocol", new ProtocolBeanDefinitionParser());

    }

    @Override
    public BeanDefinition parse(Element element, ParserContext parserContext) {
        /**
         * 在查找元素对应的bean definition解析器之前，先注册几个重要的bean definition
         * 类似于配置：<context:annotation-config>、或者<context:component-scan>
         * 注册这几个bean后，支持autowired、configuration、controller、import等常见注解
         */
        BeanDefinitionRegistry registry = parserContext.getRegistry();
        registerAnnotationConfigProcessors(registry);

        return super.parse(element, parserContext);
    }
}
