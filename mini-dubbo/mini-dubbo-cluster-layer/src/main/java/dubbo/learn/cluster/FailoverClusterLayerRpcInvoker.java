package dubbo.learn.cluster;

import dubbo.learn.common.ProviderHostAndPort;
import dubbo.learn.cluster.loadbalance.LoadBalancePolicy;
import dubbo.learn.common.RpcContext;
import dubbo.learn.common.contract.cluster.ClusterLayerRpcInvoker;
import dubbo.learn.common.contract.exchange.ExchangeLayerRpcInvoker;
import dubbo.learn.common.spi.SpiServiceLoader;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 出现失败，立即重试其他服务器。可以设置重试次数
 */
@Slf4j
@Service
public class FailoverClusterLayerRpcInvoker implements ClusterLayerRpcInvoker {

    @Autowired
    private LoadBalancePolicy loadBalancePolicy;

    @Override
    public Object invoke(List<ProviderHostAndPort> providerList, RpcContext rpcContext) {
        ExchangeLayerRpcInvoker exchangeLayerRpcInvoker =
                SpiServiceLoader.loadService(ExchangeLayerRpcInvoker.class);

        int retryTimes = 3;
        for (int i = 0; i < retryTimes; i++) {
            ProviderHostAndPort providerHostAndPort = loadBalancePolicy.selectOne(providerList);
            try {
                Object o = exchangeLayerRpcInvoker.invoke(providerHostAndPort, rpcContext);
                return o;
            } catch (Exception e) {
                log.error("fail to invoke {},exception:{},will try another",
                        providerHostAndPort,e);
                continue;
            }
        }

        throw new RuntimeException("fail times extend");
    }
}
