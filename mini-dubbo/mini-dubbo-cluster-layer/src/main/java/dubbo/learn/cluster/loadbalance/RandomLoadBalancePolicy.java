package dubbo.learn.cluster.loadbalance;

import dubbo.learn.common.ProviderHostAndPort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RandomLoadBalancePolicy implements LoadBalancePolicy {
    java.util.Random random = new java.util.Random();

    @Override
    public ProviderHostAndPort selectOne(List<ProviderHostAndPort> list) {
        int randomPos = random.nextInt(list.size());

        return list.get(randomPos);
    }
}
