package dubbo.learn.common;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ProviderHostAndPort {
    /**
     * ip
     */
    private String host;

    /**
     * 端口
     */
    private int port;


}
