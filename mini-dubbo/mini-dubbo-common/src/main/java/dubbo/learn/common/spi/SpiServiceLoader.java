package dubbo.learn.common.spi;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

@Component
@Slf4j
public class SpiServiceLoader implements ApplicationContextAware {

    private static ConcurrentHashMap<String,Object> spiName2ServiceMap = new ConcurrentHashMap<>();

    private static ApplicationContext applicationContext;


    public static <T> T loadService(Class<T> clazz) {
        Object cached = spiName2ServiceMap.get(clazz.getName());
        if (cached != null) {
            return (T) cached;
        }

        Map<String, T> map = applicationContext.getBeansOfType(clazz);
        if (CollectionUtils.isEmpty(map)) {
            return null;
        }

        if (map.size() == 1) {
            Object o = map.values().iterator().next();
            return clazz.cast(o);
        }

        String s = SpiParser.getSpiForSpecifiedService(clazz);
        if (StringUtils.isEmpty(s)) {
            log.error("发现多个服务实现bean:{},且在spi中未指定要使用的bean",map);
            throw new RuntimeException();
        }

        Object specifiedServiceInSpiFile = map.values().stream().filter(v -> Objects.equals(v.getClass().getName(), s))
                .findFirst().orElse(null);
        if (specifiedServiceInSpiFile == null) {
            log.error("spi中指定的服务在bean集合中未找到。" +
                    "发现多个服务实现bean:{},在spi中指定的服务为:{}",map,s);
            throw new RuntimeException();
        }

        spiName2ServiceMap.put(clazz.getName(),specifiedServiceInSpiFile);
        return (T) specifiedServiceInSpiFile;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        SpiServiceLoader.applicationContext = applicationContext;
    }



}
