package dubbo.learn.common.contract.cluster;

import dubbo.learn.common.ProviderHostAndPort;
import dubbo.learn.common.RpcContext;

import java.util.List;

/**
 * 注册中心层的rpc调用者
 * 1：接收上层传下来的业务参数，并返回结果
 *
 * 本层：会根据不同实现，去相应的注册中心，获取匹配的服务提供者列表，传输给下一层
 */
public interface ClusterLayerRpcInvoker {

    /**
     * 由注册中心层提供对应service的服务提供者列表，本方法可以根据负载均衡策略，进行筛选
     * @param providerList
     * @param rpcContext
     * @return
     */
    Object invoke(List<ProviderHostAndPort> providerList, RpcContext rpcContext);
}
