package dubbo.learn.common;

import lombok.Data;

import java.util.List;

@Data
public class RequestVO {
    private String requestId;

    private String serviceName;

    private String methodName;

    private Integer paramCount;

    private List<String> paramsJsonArray;
}
