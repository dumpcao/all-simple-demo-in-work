
var BBSContract = function() {
    LocalContractStorage.defineMapProperty(this, "category");
    LocalContractStorage.defineProperty(this, "categoryNums");
    LocalContractStorage.defineMapProperty(this, "categoryIndex");

    LocalContractStorage.defineMapProperty(this, "topic");
    LocalContractStorage.defineMapProperty(this, "topicContent");
    LocalContractStorage.defineMapProperty(this, "topicAdditional");
    LocalContractStorage.defineMapProperty(this, "topicAdditionalIndex");

    LocalContractStorage.defineMapProperty(this, "topicFavNums");

    LocalContractStorage.defineProperty(this, "topicNums");
    LocalContractStorage.defineMapProperty(this, "topicIndex");

    // 我打赏的
    LocalContractStorage.defineMapProperty(this, "donateInfo");
    LocalContractStorage.defineMapProperty(this, "donateNums");
    LocalContractStorage.defineMapProperty(this, "donateIndex");
    LocalContractStorage.defineMapProperty(this, "donateNas", {
        stringify: function(obj) {
            return obj.toString();
        },
        parse: function(str) {
            return new BigNumber(str);
        }
    });

    // 我收到的打赏
    LocalContractStorage.defineMapProperty(this, "getDonateNums");
    LocalContractStorage.defineMapProperty(this, "getDonateIndex");
    LocalContractStorage.defineMapProperty(this, "getDonateNas", {
        stringify: function(obj) {
            return obj.toString();
        },
        parse: function(str) {
            return new BigNumber(str);
        }
    });

    LocalContractStorage.defineMapProperty(this, "donateTopicNums");
    LocalContractStorage.defineMapProperty(this, "donateTopicIndex");
    LocalContractStorage.defineMapProperty(this, "donateTopicNas", {
        stringify: function(obj) {
            return obj.toString();
        },
        parse: function(str) {
            return new BigNumber(str);
        }
    });

    LocalContractStorage.defineMapProperty(this, "userBalance", {
        stringify: function(obj) {
            return obj.toString();
        },
        parse: function(str) {
            return new BigNumber(str);
        }
    });

    LocalContractStorage.defineMapProperty(this, "categoryTopic");

    LocalContractStorage.defineMapProperty(this, "userTopicNums");
    LocalContractStorage.defineMapProperty(this, "userTopicIndex");

    LocalContractStorage.defineProperty(this, "replyNums");
    LocalContractStorage.defineMapProperty(this, "reply");
    LocalContractStorage.defineMapProperty(this, "topicReplyNums");
    LocalContractStorage.defineMapProperty(this, "replyIndex");

    // 保存用户参与的讨论列表
    LocalContractStorage.defineMapProperty(this, "userReplyNums");
    LocalContractStorage.defineMapProperty(this, "userReplyIndex");

    LocalContractStorage.defineMapProperty(this, "userInfo");
    LocalContractStorage.defineMapProperty(this, "userAddressIndex");
    LocalContractStorage.defineMapProperty(this, "nickNameIndex");
    LocalContractStorage.defineProperty(this, "userNums");

    LocalContractStorage.defineMapProperty(this, "userFllow");
    LocalContractStorage.defineMapProperty(this, "userFllowNums");

    //收藏帖子
    LocalContractStorage.defineMapProperty(this, "favTopic"); //在某个ID，收藏的帖子信息
    LocalContractStorage.defineMapProperty(this, "favTopicMap"); //是否有收藏某个主题
    LocalContractStorage.defineMapProperty(this, "favTopicIndex"); //类似自增ID的功能
    LocalContractStorage.defineMapProperty(this, "favTopicNums"); //真实的收藏数

    //收藏栏目
    LocalContractStorage.defineMapProperty(this, "favCategory");
    LocalContractStorage.defineMapProperty(this, "favCategoryMap");
    LocalContractStorage.defineMapProperty(this, "favCategoryIndex");
    LocalContractStorage.defineMapProperty(this, "favCategoryNums");

    //被用户关注的数量
    LocalContractStorage.defineMapProperty(this, "userFansNums");

    LocalContractStorage.defineMapProperty(this, "dailyUser");
    LocalContractStorage.defineMapProperty(this, "dailyTopic");
    LocalContractStorage.defineMapProperty(this, "dailyReply");

    // 黑名单
    LocalContractStorage.defineMapProperty(this, "blacklist");

    LocalContractStorage.defineMapProperty(this, "dailyUserNums");
    LocalContractStorage.defineMapProperty(this, "dailyTopicNums");
    LocalContractStorage.defineMapProperty(this, "dailyReplyNums");

    LocalContractStorage.defineProperty(this, "hotTopicCache");
    LocalContractStorage.defineProperty(this, "hotTopic");

    LocalContractStorage.defineProperty(this, "taxNas", {
        stringify: function(obj) {
            return obj.toString();
        },
        parse: function(str) {
            return new BigNumber(str);
        }
    });

    LocalContractStorage.defineProperty(this, "config");
    LocalContractStorage.defineProperty(this, "adsList");

    LocalContractStorage.defineProperty(this, "adminAddress");
};