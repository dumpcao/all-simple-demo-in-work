package com.phei.netty.pooledbio;

import java.util.concurrent.*;

/**
 * desc:
 *
 * @author : caokunliang
 * creat_date: 2018/5/15 0015
 * creat_time: 13:59
 **/
public class TimeServerHandlerExecutePool {
    private ExecutorService executorService;

    public TimeServerHandlerExecutePool(int maxPoolSize, int queueSize) {
        executorService = new ThreadPoolExecutor(2, 3,
                120L, TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(1)
//                new RejectedExecutionHandler() {
//            @Override
//            public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
//                System.out.println(r);
//            }
//        }
        );
    }

    public void execute(Runnable task){
        executorService.execute(task);
    }
}
