package com.phei.netty.pooledbio;

import com.phei.netty.bio.TimeServerHandler;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * desc:
 *
 * @author : caokunliang
 * creat_date: 2018/5/15 0015
 * creat_time: 13:56
 **/
public class TimeServer {
    public static void main(String[] args) {
        int port = 8080;
        if (args != null && args.length > 0){
            try {
                port = Integer.valueOf(args[0]);
            } catch (Exception e) {

            }
        }

        ServerSocket server = null;
        try {
            server = new ServerSocket(port);
            System.out.println("The time server is start in port: "  + port);
            Socket socket = null;
            TimeServerHandlerExecutePool pool = new TimeServerHandlerExecutePool(50, 10000);

            while (true){
                socket = server.accept();
                pool.execute(new TimeServerHandler(socket));
                System.out.println("put over,wait next");
            }
        } catch (Exception e) {
            if (server != null){
                System.out.println("The time server close");
                try {
                    server.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                server = null;
            }
        }
    }
}
