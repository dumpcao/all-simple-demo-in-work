package com.example.webdemo;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class WebDemoApplication {

	public static void main(String[] args) throws InterruptedException {
        MusicPlayer player=new MusicPlayer();
        try {
            player.play_mp3("D:\\CloudMusic\\一直很安静.mp3");
        } catch (UnsupportedAudioFileException | IOException e) {
            e.printStackTrace();
        }
        CountDownLatch count = new CountDownLatch(1);
        ScheduledThreadPoolExecutor executor = new ScheduledThreadPoolExecutor(3);
        executor.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                String url = "https://detail.tmall.com/item.htm?spm=a1z10.1-b.w5003-22439709729.1.98697dab3jEm9c&id=558596461970&scene=taobao_shop";
                RestTemplate restTemplate = new RestTemplate();
                ResponseEntity<String> s =
                        restTemplate.postForEntity(url, new Object(), String.class);
                String body = s.getBody();
                if (!body.contains("已下架")) {
                    MusicPlayer player=new MusicPlayer();
                    try {
                        player.play_mp3("D:\\CloudMusic\\一直很安静.mp3");
                    } catch (UnsupportedAudioFileException | IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        },0L,3, TimeUnit.SECONDS);

        count.await();

    }

}
