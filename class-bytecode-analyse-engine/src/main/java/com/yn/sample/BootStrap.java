package com.yn.sample;

import com.yn.sample.domain.ClassInfo;
import com.yn.sample.execution.MethodExecutionEngine;
import com.yn.sample.parser.JavapClassFileParser;
import com.yn.sample.util.JavapUtil;
import com.yn.sample.util.OpCodeEnum;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
@ComponentScan("com.yn.sample")
public class BootStrap {
    public static void main(String[] args) throws Throwable {
        /**
         * 1 反编译指定class,获取反编译结果
         */
        String javapResult = JavapUtil.invokeJavap(CheckAndSet.class);

        /**
         * 2 解析文件
         */
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(BootStrap.class);
        JavapClassFileParser javapClassFileParser = context.getBean(JavapClassFileParser.class);
        ClassInfo classInfo = javapClassFileParser.parse(javapResult);

        CheckAndSet target = new CheckAndSet();
        MethodExecutionEngine executionEngine = context.getBean(MethodExecutionEngine.class);
//
//        {
//            /**
//             * 传递一个变量，2
//             */
//            ArrayList<Object> arguments = new ArrayList<>();
//            arguments.add(12);
//            Object o = executionEngine.execute(target, "checkAndSetF1(int)", classInfo, arguments);
//            System.out.println("result:" + o);
//        }


        CheckAndSet checkAndSet = new CheckAndSet();
        checkAndSet.checkAndSetF(12);
        {
            /**
             * 传递一个变量，2
             */
            ArrayList<Object> arguments = new ArrayList<>();
            arguments.add(12);
            Object o = executionEngine.execute(target, "checkAndSetF(int)", classInfo, arguments);
            System.out.println("result:" + o);
        }


    }
}
