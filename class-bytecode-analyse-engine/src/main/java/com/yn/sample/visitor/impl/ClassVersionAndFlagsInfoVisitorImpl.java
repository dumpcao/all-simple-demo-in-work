package com.yn.sample.visitor.impl;

import com.yn.sample.visitor.ClassVersionAndFlagsInfoVisitor;
import org.springframework.stereotype.Component;

@Component
public class ClassVersionAndFlagsInfoVisitorImpl implements ClassVersionAndFlagsInfoVisitor {

    @Override
    public void visitClassName(String fileName) {

    }

    @Override
    public void visitMinorVersion(Integer minorVersion) {

    }

    @Override
    public void visitMajorVersion(Integer majorVersion) {

    }

    @Override
    public void visitFlags(String flags) {

    }
}
