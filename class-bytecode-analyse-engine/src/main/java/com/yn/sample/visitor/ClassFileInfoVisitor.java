package com.yn.sample.visitor;

public interface ClassFileInfoVisitor {
    void visitOriginClassFileName(String fileName);

    void visitLastModified(String date);

    void visitSize(Long size);

    void visitMd5(Long md5);

    void visitCompiledSourceFileName(String sourceJavaFileName);
}
