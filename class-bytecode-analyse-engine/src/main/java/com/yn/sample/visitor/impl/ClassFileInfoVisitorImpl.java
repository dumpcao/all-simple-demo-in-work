package com.yn.sample.visitor.impl;

import com.yn.sample.visitor.ClassFileInfoVisitor;
import org.springframework.stereotype.Component;

@Component
public class ClassFileInfoVisitorImpl implements ClassFileInfoVisitor {
    @Override
    public void visitOriginClassFileName(String fileName) {

    }

    @Override
    public void visitLastModified(String date) {

    }

    @Override
    public void visitSize(Long size) {

    }

    @Override
    public void visitMd5(Long md5) {

    }

    @Override
    public void visitCompiledSourceFileName(String sourceJavaFileName) {

    }
}
