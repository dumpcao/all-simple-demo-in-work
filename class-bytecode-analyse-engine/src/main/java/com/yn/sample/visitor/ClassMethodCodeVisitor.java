package com.yn.sample.visitor;

import com.yn.sample.domain.MethodCodeStackSizeAndLocalVariablesTableSize;
import com.yn.sample.domain.MethodInstructionVO;

import java.util.List;

public interface ClassMethodCodeVisitor {
    /**
     * 方法的操作数栈、本地局部变量表、方法入参表的大小
     * @param vo
     */
    void visitMethodStackSizeAndLocalVariablesTableSizeAndArgsSize(MethodCodeStackSizeAndLocalVariablesTableSize vo);


    void visitInstructionStart();

    void visitInstruction(MethodInstructionVO methodInstructionVO);

    void visitInstructionEnd();


    List<MethodInstructionVO> getInstructions();

    void newInstructionList();

    MethodCodeStackSizeAndLocalVariablesTableSize getMethodCodeStackSizeAndLocalVariablesTableSize();
}
