package com.yn.sample.visitor;

import com.yn.sample.domain.ConstantPoolItem;

import java.util.ArrayList;

public interface ClassConstantPoolInfoVisitor {
    /**
     * 常量池解析开始
     */
    void visitConstantPoolStarted();

    /**
     * 解析到每一个常量池对象时，回调本方法
     * @param constantPoolItem
     */
    void visitConstantPoolItem(ConstantPoolItem constantPoolItem);

    /**
     * 常量池解析结束
     */
    void visitConstantPoolEnd();

    /**
     * 获取最终的常量池对象
     * @return
     */
    ArrayList<ConstantPoolItem> getConstantPoolItemList();
}
