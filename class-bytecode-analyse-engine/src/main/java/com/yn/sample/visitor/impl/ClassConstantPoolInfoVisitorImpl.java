package com.yn.sample.visitor.impl;

import com.yn.sample.visitor.ClassConstantPoolInfoVisitor;
import com.yn.sample.domain.ConstantPoolItem;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class ClassConstantPoolInfoVisitorImpl implements ClassConstantPoolInfoVisitor {
    ArrayList<ConstantPoolItem> list = new ArrayList<>();

    @Override
    public void visitConstantPoolStarted() {

    }

    @Override
    public void visitConstantPoolItem(ConstantPoolItem constantPoolItem) {
        System.out.println("visitConstantPoolItem:" + constantPoolItem);
        list.add(constantPoolItem);
    }

    @Override
    public void visitConstantPoolEnd() {

    }

    @Override
    public ArrayList<ConstantPoolItem> getConstantPoolItemList() {
        return list;
    }
}
