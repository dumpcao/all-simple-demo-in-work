package com.yn.sample.visitor.impl;

import com.yn.sample.domain.MethodCodeStackSizeAndLocalVariablesTableSize;
import com.yn.sample.visitor.ClassMethodCodeVisitor;
import com.yn.sample.domain.MethodInstructionVO;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ClassMethodCodeVisitorImpl implements ClassMethodCodeVisitor {

    MethodCodeStackSizeAndLocalVariablesTableSize methodCodeStackSizeAndLocalVariablesTableSize;

    @Override
    public void visitMethodStackSizeAndLocalVariablesTableSizeAndArgsSize(MethodCodeStackSizeAndLocalVariablesTableSize vo) {
        this.methodCodeStackSizeAndLocalVariablesTableSize = vo;
    }

    @Override
    public void visitInstructionStart() {

    }

    ArrayList<MethodInstructionVO> instructions = new ArrayList<>();

    @Override
    public void visitInstruction(MethodInstructionVO methodInstructionVO) {
        System.out.println("visitInstruction:" + methodInstructionVO);

        instructions.add(methodInstructionVO);
    }

    @Override
    public void visitInstructionEnd() {

    }

    /**
     * 获取方法的字节码指令集合
     * @return
     */
    @Override
    public List<MethodInstructionVO> getInstructions() {
        return instructions;
    }

    /**
     * 获取方法的字节码指令集合
     * @return
     */
    @Override
    public void newInstructionList() {
        instructions = new ArrayList<>();
    }

    @Override
    public MethodCodeStackSizeAndLocalVariablesTableSize getMethodCodeStackSizeAndLocalVariablesTableSize() {
        return methodCodeStackSizeAndLocalVariablesTableSize;
    }
}
