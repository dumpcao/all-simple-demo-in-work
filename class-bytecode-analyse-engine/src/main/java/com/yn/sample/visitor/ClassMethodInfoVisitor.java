package com.yn.sample.visitor;

import com.yn.sample.domain.MethodInfo;
import com.yn.sample.visitor.ClassMethodCodeVisitor;

import java.util.LinkedHashMap;

public interface ClassMethodInfoVisitor {
    void visitMethodStart();

    void visitMethodName(String name);

    void visitMethodDescriptor(String descriptor);

    void visitFlags(String flags);

    ClassMethodCodeVisitor visitMethodCode();

    void visitMethodEnd();

    LinkedHashMap<String, MethodInfo> getMethodInfoMap();
}
