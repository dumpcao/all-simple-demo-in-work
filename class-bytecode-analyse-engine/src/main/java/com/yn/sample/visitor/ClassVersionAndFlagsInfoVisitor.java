package com.yn.sample.visitor;


public interface ClassVersionAndFlagsInfoVisitor {
    void visitClassName(String fileName);


    void visitMinorVersion(Integer minorVersion);

    void visitMajorVersion(Integer majorVersion);

    void visitFlags(String flags);

}
