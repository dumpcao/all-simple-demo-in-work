package com.yn.sample.util;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class FileReaderUtil {
    public static void main(String[] args) {

    }

    public static List<String> readFile2Lines(String filePath) {
        File file = new File(filePath);
        try {
            List<String> lines = FileUtils.readLines(file, "utf-8");
            return lines;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 将整个文件内容，转换为按行的数组
     * @param fileContent
     * @return
     */
    public static List<String> convertWholeFileContent2LineArray(String fileContent) {
        String[] array = fileContent.split("\r\n");
        List<String> list = Arrays.asList(array);
        return list;
    }
}
