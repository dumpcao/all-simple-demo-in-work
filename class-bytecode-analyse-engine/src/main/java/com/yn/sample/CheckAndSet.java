package com.yn.sample;

import lombok.Data;

@Data
public class CheckAndSet {
    private int f;
    private String name;
    private CheckAndSet next;
    public static final int constantInt = 333;

    public void checkAndSetF(int f) {
        if (f >= 0) {
//            this.f = f;
            this.next = new CheckAndSet();
            this.next.f = 123;
            this.next.name = "test user";
        } else {
            throw new IllegalArgumentException();
        }
    }

    public boolean checkAndSetF1(int f) {
        boolean a = true;
        boolean b = f >= 0;
        return b;
    }
}
