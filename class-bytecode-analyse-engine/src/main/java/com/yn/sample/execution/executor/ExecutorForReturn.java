package com.yn.sample.execution.executor;

import com.yn.sample.domain.InstructionExecutionResult;
import com.yn.sample.execution.InstructionExecutionContext;
import com.yn.sample.util.OpCodeEnum;
import org.springframework.stereotype.Component;

@Component
public class ExecutorForReturn  implements ExecutorByOpCode{

    @Override
    public String getOpCode() {
        return OpCodeEnum.RETURN.name();
    }

    /**
     * todo:判断是否需要返回
     * @param context
     * @return
     */
    @Override
    public InstructionExecutionResult execute(InstructionExecutionContext context) {
//        Object last = context.getOperandStack().getLast();
//        System.out.println("result:"+ last);
        return new InstructionExecutionResult(null,null,true);
    }
}
