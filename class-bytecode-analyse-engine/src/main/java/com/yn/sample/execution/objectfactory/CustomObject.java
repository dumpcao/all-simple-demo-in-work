package com.yn.sample.execution.objectfactory;

import com.yn.sample.execution.memory.MemoryManagement;
import lombok.Data;

@Data
public class CustomObject {
    /**
     * 对象头
     */
    private CustomObjectHeader header = new CustomObjectHeader();
    private final ObjectInfo objectInfo;


    public CustomObject(ClassInfo classInfo) {
        header.setClassInfo(classInfo);
        int totalSize = classInfo.getTotalSize();

        objectInfo = MemoryManagement.allocateBytebuffer(totalSize);
    }
}
