package com.yn.sample.execution.executor;

import com.yn.sample.domain.InstructionExecutionResult;
import com.yn.sample.execution.InstructionExecutionContext;
import com.yn.sample.util.OpCodeEnum;
import org.springframework.stereotype.Component;

@Component
public class ExecutorForIConst0 extends BaseExecutorForIConstN implements ExecutorByOpCode{

    @Override
    public String getOpCode() {
        return OpCodeEnum.iconst_0.name();
    }

    @Override
    public InstructionExecutionResult execute(InstructionExecutionContext context) {
        super.execute(context, 0);
        return null;
    }
}
