package com.yn.sample.execution.objectfactory;

import lombok.Data;

import java.util.Map;

@Data
public class ClassInfo {
    private Class<?> clazz;

    /**
     * 各个field在buffer中的offset等信息
     */
    Map<String, FieldInfo> fieldInfoMap;

    /**
     *
     */
    int totalSize;
}
