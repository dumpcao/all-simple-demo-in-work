package com.yn.sample.execution.objectfactory;

import lombok.Data;

import java.nio.ByteBuffer;

@Data
public class ObjectInfo {

    /**
     * 在bytebuffer中的position
     */
    private int objectOffsetInByteBuffer;

    /**
     * 对象占用的字节大小
     */
    private int size;

    /**
     * eden区域
     */
    ByteBuffer eden;

}
