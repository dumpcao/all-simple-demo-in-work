package com.yn.sample.execution.executor;

import com.yn.sample.domain.InstructionExecutionResult;
import com.yn.sample.execution.InstructionExecutionContext;
import com.yn.sample.util.OpCodeEnum;
import org.springframework.stereotype.Component;

/**
 * 0x15 iload 将指定的 int 型局部变量推送至栈顶。
 */
@Component
public class ExecutorForILoad3 extends BaseExecutorForILoadN implements ExecutorByOpCode{

    @Override
    public String getOpCode() {
        return OpCodeEnum.iload_3.name();
    }

    @Override
    public InstructionExecutionResult execute(InstructionExecutionContext context) {
        super.execute(context,3);
        return null;
    }
}
