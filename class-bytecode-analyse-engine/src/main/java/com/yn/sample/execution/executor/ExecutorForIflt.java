package com.yn.sample.execution.executor;

import com.yn.sample.domain.InstructionExecutionResult;
import com.yn.sample.domain.MethodInstructionVO;
import com.yn.sample.execution.InstructionExecutionContext;
import com.yn.sample.util.OpCodeEnum;
import org.springframework.stereotype.Component;

@Component
public class ExecutorForIflt implements ExecutorByOpCode{

    @Override
    public String getOpCode() {
        return OpCodeEnum.iflt.name();
    }

    @Override
    public InstructionExecutionResult execute(InstructionExecutionContext context) {
        MethodInstructionVO instructionVO = context.getInstructionVO();

        int o = (int) context.getOperandStack().removeLast();
        if (o < 0) {
            //需要跳转
            return new InstructionExecutionResult(instructionVO.getOperand());
        }

        return null;
    }
}
