package com.yn.sample.execution.objectfactory;

import lombok.Data;

import java.lang.reflect.Field;

@Data
public class FieldInfo {
    private String fieldName;

    private Integer fieldOffset;

    private Field field;
}
