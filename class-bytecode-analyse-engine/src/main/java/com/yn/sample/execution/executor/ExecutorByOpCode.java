package com.yn.sample.execution.executor;

import com.yn.sample.domain.InstructionExecutionResult;
import com.yn.sample.execution.InstructionExecutionContext;

public interface ExecutorByOpCode {
    String getOpCode();

    /**
     *
     * @param context
     * @return 如果需要跳转，则返回要跳转的指令的偏移量；否则返回null
     */
    InstructionExecutionResult execute(InstructionExecutionContext context);
}
