package com.yn.sample.execution.executor;

import com.yn.sample.domain.ConstantPoolItem;
import com.yn.sample.domain.InstructionExecutionResult;
import com.yn.sample.execution.InstructionExecutionContext;
import com.yn.sample.util.ConstantPoolUtil;
import com.yn.sample.util.OpCodeEnum;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ExecutorForLdc  implements ExecutorByOpCode{

    @Override
    public String getOpCode() {
        return OpCodeEnum.ldc.name();
    }

    @Override
    public InstructionExecutionResult execute(InstructionExecutionContext context) {
        String operand = context.getInstructionVO().getOperand();
        List<ConstantPoolItem> constantPoolItems = context.getConstantPoolItems();
        String value = ConstantPoolUtil.getConstantPoolItemValue(constantPoolItems, operand);

        context.getOperandStack().addLast(value);
        return null;
    }
}
