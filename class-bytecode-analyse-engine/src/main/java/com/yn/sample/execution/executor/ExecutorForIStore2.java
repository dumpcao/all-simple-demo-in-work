package com.yn.sample.execution.executor;

import com.yn.sample.domain.InstructionExecutionResult;
import com.yn.sample.execution.InstructionExecutionContext;
import com.yn.sample.util.OpCodeEnum;
import org.springframework.stereotype.Component;

@Component
public class ExecutorForIStore2 extends BaseExecutorForIStoreN implements ExecutorByOpCode{

    @Override
    public String getOpCode() {
        return OpCodeEnum.istore_2.name();
    }

    @Override
    public InstructionExecutionResult execute(InstructionExecutionContext context) {
        super.execute(context,2);
        return null;
    }
}
