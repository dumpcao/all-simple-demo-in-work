package com.yn.sample.execution.executor;

import com.yn.sample.execution.InstructionExecutionContext;
import com.yn.sample.util.OpCodeEnum;
import org.springframework.stereotype.Component;


public abstract class BaseExecutorForIStoreN {


    public void execute(InstructionExecutionContext context,Integer slotNum) {
        // 栈顶元素
        Object o = context.getOperandStack().removeLast();

        // 放到本地变量表
        context.getLocalVariables().add(slotNum,o);
    }
}
