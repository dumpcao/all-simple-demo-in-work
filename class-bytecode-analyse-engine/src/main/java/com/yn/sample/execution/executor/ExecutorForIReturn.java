package com.yn.sample.execution.executor;

import com.yn.sample.domain.InstructionExecutionResult;
import com.yn.sample.execution.InstructionExecutionContext;
import com.yn.sample.util.OpCodeEnum;
import org.springframework.stereotype.Component;

@Component
public class ExecutorForIReturn implements ExecutorByOpCode{

    @Override
    public String getOpCode() {
        return OpCodeEnum.ireturn.name();
    }

    /**
     * @param context
     * @return
     */
    @Override
    public InstructionExecutionResult execute(InstructionExecutionContext context) {
        Object last = context.getOperandStack().removeLast();
        System.out.println("result:"+ last);

        InstructionExecutionResult result = new InstructionExecutionResult(null,last,true);
        return result;
    }
}
