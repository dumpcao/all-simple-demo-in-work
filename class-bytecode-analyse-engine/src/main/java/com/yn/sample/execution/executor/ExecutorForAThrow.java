package com.yn.sample.execution.executor;

import com.yn.sample.domain.ConstantPoolItem;
import com.yn.sample.domain.ConstantPoolItemTypeEnum;
import com.yn.sample.domain.InstructionExecutionResult;
import com.yn.sample.execution.InstructionExecutionContext;
import com.yn.sample.util.OpCodeEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
@Slf4j
public class ExecutorForAThrow implements ExecutorByOpCode{

    @Override
    public String getOpCode() {
        return OpCodeEnum.athrow.name();
    }

    @Override
    public InstructionExecutionResult execute(InstructionExecutionContext context) {
        /**
         * 获取栈顶的异常对象的引用
         */
        Object o = context.getOperandStack().removeLast();

        /**
         * 我们这里的demo没有异常处理器，直接抛出
         */
        InstructionExecutionResult result = new InstructionExecutionResult();
        result.setExceptional(true);
        result.setResult(o);
        return result;
    }
}
