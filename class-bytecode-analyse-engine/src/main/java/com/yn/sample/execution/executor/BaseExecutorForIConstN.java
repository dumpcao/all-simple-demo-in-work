package com.yn.sample.execution.executor;

import com.yn.sample.execution.InstructionExecutionContext;
import com.yn.sample.util.OpCodeEnum;
import org.springframework.stereotype.Component;

public class BaseExecutorForIConstN {

    public void execute(InstructionExecutionContext context,Integer counter) {
        context.getOperandStack().addLast(counter);
    }
}
