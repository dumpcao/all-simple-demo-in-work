package com.yn.sample.execution.executor;

import com.yn.sample.domain.ConstantPoolItem;
import com.yn.sample.domain.ConstantPoolItemTypeEnum;
import com.yn.sample.domain.InstructionExecutionResult;
import com.yn.sample.execution.InstructionExecutionContext;
import com.yn.sample.util.OpCodeEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
@Slf4j
public class ExecutorForDup implements ExecutorByOpCode{

    @Override
    public String getOpCode() {
        return OpCodeEnum.dup.name();
    }

    @Override
    public InstructionExecutionResult execute(InstructionExecutionContext context) {


        // 复制栈顶对象，如果是
        Object last = context.getOperandStack().getLast();
        context.getOperandStack().addLast(last);


        return null;
    }
}
