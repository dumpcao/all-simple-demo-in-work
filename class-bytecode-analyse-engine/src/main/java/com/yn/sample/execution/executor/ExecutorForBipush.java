package com.yn.sample.execution.executor;

import com.yn.sample.domain.InstructionExecutionResult;
import com.yn.sample.execution.InstructionExecutionContext;
import com.yn.sample.util.OpCodeEnum;
import org.springframework.stereotype.Component;

@Component
public class ExecutorForBipush implements ExecutorByOpCode{

    @Override
    public String getOpCode() {
        return OpCodeEnum.bipush.name();
    }

    @Override
    public InstructionExecutionResult execute(InstructionExecutionContext context) {
        String operand = context.getInstructionVO().getOperand();
        context.getOperandStack().addLast(Integer.valueOf(operand));

        return null;
    }
}
