package com.yn.sample.execution.executor;

import com.yn.sample.execution.InstructionExecutionContext;
import com.yn.sample.util.OpCodeEnum;
import org.springframework.stereotype.Component;

/**
 * 0x15 iload 将指定的 int 型局部变量推送至栈顶。
 */
@Component
public class BaseExecutorForILoadN {

    public void execute(InstructionExecutionContext context,Integer slotNum) {
        Object variable = context.getLocalVariables().get(slotNum);

        context.getOperandStack().addLast(variable);
    }
}
