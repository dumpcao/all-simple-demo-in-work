package com.yn.sample.execution;

import com.yn.sample.domain.ConstantPoolItem;
import com.yn.sample.domain.MethodInstructionVO;
import lombok.Data;

import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

@Data
public class InstructionExecutionContext {
    /**
     * 待执行方法的target，即：this
     */
    Object target;

    /**
     * 常量池
     */
    List<ConstantPoolItem> constantPoolItems;

    /**
     * 本地变量表
     */
    List<Object> localVariables;

    /**
     * 操作数栈
     */
    LinkedList<Object> operandStack;

    /**
     * 当前的指令
     */
    MethodInstructionVO instructionVO;
}
