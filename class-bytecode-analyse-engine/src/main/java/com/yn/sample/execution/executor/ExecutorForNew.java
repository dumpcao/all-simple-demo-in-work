package com.yn.sample.execution.executor;

import com.yn.sample.domain.ConstantPoolItem;
import com.yn.sample.domain.ConstantPoolItemTypeEnum;
import com.yn.sample.domain.InstructionExecutionResult;
import com.yn.sample.execution.InstructionExecutionContext;
import com.yn.sample.util.OpCodeEnum;
import com.yn.sample.util.ParseEngineHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.util.Objects;

@Component
@Slf4j
public class ExecutorForNew implements ExecutorByOpCode{

    @Override
    public String getOpCode() {
        return OpCodeEnum.NEW.name();
    }

    @Override
    public InstructionExecutionResult execute(InstructionExecutionContext context) {
        /**
         * 获取操作数，操作数是常量池的符号引用；比如
         *  6: putfield      #2                  // Field f:I
         *  其中，#2在常量池中：
         *  #2 = Fieldref           #5.#27         // com/yn/sample/CheckAndSet.f:I
         */
        String operand = context.getInstructionVO().getOperand();
        /**
         * 根据operand，查找常量池，找到对应的符号引用
         */
        ConstantPoolItem poolItem = context.getConstantPoolItems().stream().filter(constantPoolItem -> Objects.equals(constantPoolItem.getId(), operand))
                .findFirst().orElse(null);
        if (poolItem == null) {
            throw new RuntimeException();
        }

        boolean b = Objects.equals(poolItem.getConstantPoolItemTypeEnum(), ConstantPoolItemTypeEnum.Class);
        if (!b) {
            throw new RuntimeException();
        }

        /**
         * 一般comment就是这一段内容：
         * java/lang/IllegalArgumentException
         */
        String comment = poolItem.getComment();
        String className = comment.replaceAll("/", ".");

        Class<?> clazz;
        Object object;
        try {
            clazz = Class.forName(className);
            object = clazz.newInstance();
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            log.error("e:{}", e);
            throw new RuntimeException(e);
        }

        // 入栈
        context.getOperandStack().addLast(object);


        return null;
    }
}
