package com.yn.sample.execution.objectfactory;

import lombok.Data;

@Data
public class CustomObjectHeader {
    private ClassInfo classInfo;

}
