package com.yn.sample.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

/**
 *
 * 每个方法会有对应的一个本类实例。
 * 用于保存如下数据：
 * stack=1, locals=1, args_size=1
 *
 */
@Data
public class MethodCodeStackSizeAndLocalVariablesTableSize {

    /**
     * 操作数栈的大小
     */
    private Integer stackSize;

    /**
     * 本地局部变量表的大小
     */
    private Integer localVariablesSize;
    /**
     * 方法实参的个数
     */
    private Integer argumentsSize;


}
