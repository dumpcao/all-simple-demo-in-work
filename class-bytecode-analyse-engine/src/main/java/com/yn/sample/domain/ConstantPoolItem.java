package com.yn.sample.domain;

import lombok.Data;

/**
 * javap反编译后，一般如下：
 * Classfile /F:/gitee-ckl/all-simple-demo-in-work/asm-demo/target/classes/com/yn/method/CheckAndSet.class
 *   Last modified 2020-3-28; size 653 bytes
 *   MD5 checksum 68555771eebee43e5adc7f5724700196
 *   Compiled from "CheckAndSet.java"
 * public class com.yn.method.CheckAndSet
 *   SourceFile: "CheckAndSet.java"
 *   minor version: 0
 *   major version: 52
 *   flags: ACC_PUBLIC, ACC_SUPER
 * Constant pool:
 *    #1 = Methodref          #6.#25         //  java/lang/Object."<init>":()V
 *    #2 = Fieldref           #5.#26         //  com/yn/method/CheckAndSet.f:I
 *
 *    本类主要对应以下的每一行：
 *    #1 = Methodref          #6.#25         //  java/lang/Object."<init>":()V
 *    #2 = Fieldref           #5.#26         //  com/yn/method/CheckAndSet.f:I
 */
@Data
public class ConstantPoolItem {
    /**
     * 格式如：
     * #1
     */
    private String id;

    /**
     * 如：
     * Methodref
     */
    private ConstantPoolItemTypeEnum constantPoolItemTypeEnum;

    /**
     * #6.#25
     */
    private String value;

    /**
     * 对于value的注释，因为value字段一般就是对常量池的id引用，
     * javap反编译后，为了方便大家阅读，这里会显示为相应的常量
     */
    private String comment;
}
