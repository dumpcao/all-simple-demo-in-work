package com.yn.sample.domain;

import lombok.Data;

import java.util.List;

/**
 *
 * MethodInfo中会封装方法的全部信息，也就是class反编译后的如下部分：
 * public com.yn.method.CheckAndSet();
 *     descriptor: ()V
 *     flags: ACC_PUBLIC
 *     Code:
 *       stack=1, locals=1, args_size=1
 *          0: aload_0
 *          1: invokespecial #1                  // Method java/lang/Object."<init>":()V
 *          4: return
 *       LineNumberTable:
 *         line 3: 0
 *       LocalVariableTable:
 *         Start  Length  Slot  Name   Signature
 *             0       5     0  this   Lcom/yn/method/CheckAndSet;
 */
@Data
public class MethodInfo {
    /**
     * 方法签名
     */
    private String signature;


    /**
     * 方法的code区域，也就是指令集合
     */
    private List<MethodInstructionVO> instructionVOList;

    /**
     * 方法的操作数栈、本地局部变量表、方法入参表的大小
     */
    MethodCodeStackSizeAndLocalVariablesTableSize methodCodeStackSizeAndLocalVariablesTableSize;
}
