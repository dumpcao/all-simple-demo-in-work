package com.yn.sample.domain;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

@Data
public class MethodInstructionVO {
    /**
     * 序列号
     */
    private String sequenceNumber;

    /**
     * 操作码
     */
    private String opcode;

    /**
     * 操作码的说明
     */
    private String opCodeDesc;

    /**
     * 操作数
     */
    private String operand;

    /**
     * 操作数的说明
     */
    private String comment;

    /**
     * 按顺序执行的情况下的下一条指令，比如，javap反编译后，字节码如下：
     *          0: iconst_1
     *          1: istore_2
     *          2: iload_1
     *          3: iflt          10
     *          6: iconst_1
     *          7: goto          11
     * 那么，0: iconst_1 这条指令的nextInstruction就会执行偏移为1的那个；
     */
    @JSONField(serialize = false)
    MethodInstructionVO nextInstruction;
}
