package com.yn.sample.domain;

import lombok.Data;

import java.util.LinkedHashMap;
import java.util.List;

@Data
public class ClassInfo {
    /**
     * 方法集合
     */
    private LinkedHashMap<String, MethodInfo> methodInfoMap;

    /**
     * 常量池item集合
     */
    private List<ConstantPoolItem> constantPoolItems;
}
