package com.yn.sample.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class InstructionExecutionResult {
    /**
     * 要跳转的指令的sequenceNum
     */
    private String instructionSequenceNum;

    /**
     * 当指令为ireturn 这类指令，需要返回结果
     */
    private Object result;


    private boolean isReturnInstruction;

    private boolean isExceptional;


    public InstructionExecutionResult(String instructionSequenceNum) {
        this.instructionSequenceNum = instructionSequenceNum;
    }

    public InstructionExecutionResult(String instructionSequenceNum, Object result, Boolean isReturn) {
        this.instructionSequenceNum = instructionSequenceNum;
        this.result = result;
        this.isReturnInstruction = isReturn;
    }




}
