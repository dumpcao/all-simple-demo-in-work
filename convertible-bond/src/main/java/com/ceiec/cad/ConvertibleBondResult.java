package com.ceiec.cad;

import lombok.Data;

import java.util.List;

@Data
public class ConvertibleBondResult {
    private int page;

    private List<ConvertibleBondItem> rows;

}
