package com.ceiec.cad;

import lombok.Data;

@Data
public class ConvertibleBondItem {
    private String id;

    private ConvertibleBondItemCell cell;
}
