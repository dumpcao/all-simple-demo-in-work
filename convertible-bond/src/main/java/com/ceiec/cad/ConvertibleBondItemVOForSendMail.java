package com.ceiec.cad;

import lombok.Data;

@Data
public class ConvertibleBondItemVOForSendMail {

    /**
     * 可转债的id，如113574
     */
    private String bond_id;

    /**
     * 可转债的名称，如华体转债
     */
    private String bond_nm;

    /**
     * 申购日期 ，如"2020-03-31"
     */
    private String apply_date;

    /**
     * 评级，如A+
     */
    private String rating_cd;

    /**
     * 申购建议
     */
    private String jsl_advise_text;

    /**
     * 包含了“2020-03-31申购”字符串的那个字段
     */
    private String progress_nm;

}
