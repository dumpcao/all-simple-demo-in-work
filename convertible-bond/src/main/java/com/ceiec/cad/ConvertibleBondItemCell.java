package com.ceiec.cad;

import lombok.Data;

@Data
public class ConvertibleBondItemCell {

    /**
     * price : 28.00
     * increase_rt : 4.13
     * pma_rt : 102.12
     * pb : 2.507
     * rid : 631
     * stock_id : 603579
     * stock_nm : 荣泰健康
     * bond_id :
     * bond_nm : null
     * amount : 6.00
     * b_shares : 0.0
     * cb_amount : 15.31
     * ma20_price : 27.42
     * convert_price : 27.42
     * apply_date : null
     * apply_cd : null
     * ration_cd : null
     * list_date : null
     * status_cd : ON
     * ration_rt : null
     * online_amount : null
     * lucky_draw_rt : null
     * individual_limit : 10000
     * underwriter_rt : null
     * rating_cd : null
     * offline_limit : null
     * offline_accounts : null
     * offline_draw : null
     * jsl_advise_text :
     * progress_nm : 董事会预案
     * cb_type : 可转债
     * progress_dt : 2020-03-27
     * cp_flag : N
     * apply_tips :
     * ap_flag : N
     */

    private String price;
    private String increase_rt;
    private String pma_rt;
    private String pb;
    private int rid;
    private String stock_id;
    private String stock_nm;
    /**
     * 可转债的id，如113574
     */
    private String bond_id;
    /**
     * 可转债的名称，如华体转债
     */
    private String bond_nm;

    private String amount;
    private String b_shares;
    private String cb_amount;
    private String ma20_price;
    private String convert_price;
    /**
     * 申购日期 ，如"2020-03-31"
     */
    private String apply_date;
    private Object apply_cd;
    private Object ration_cd;
    private Object list_date;
    private String status_cd;
    private Object ration_rt;
    private Object online_amount;
    private Object lucky_draw_rt;
    private int individual_limit;
    private Object underwriter_rt;
    /**
     * 评级，如A+
     */
    private String rating_cd;
    private Object offline_limit;
    private Object offline_accounts;
    private Object offline_draw;
    /**
     * 申购建议
     */
    private String jsl_advise_text;
    /**
     * 包含了“2020-03-31申购”字符串的那个字段
     */
    private String progress_nm;
    private String cb_type;
    private String progress_dt;
    private String cp_flag;
    private String apply_tips;
    private String ap_flag;


}
