package com.ceiec.cad.scheduledtask;

import com.alibaba.fastjson.JSONObject;
import com.ceiec.cad.*;
import com.ceiec.cad.util.DateUtils;
import example.EmailTest;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Component
@Slf4j
public class CheckPendingDisposalCountTask {
//    @Autowired
//    private RestTemplate restTemplate;


    public static final String url = "https://www.jisilu.cn/data/cbnew/pre_list/?___jsl=LST___t=1585271563696";


    @Scheduled(cron = "${cron1}")
    @Scheduled(cron = "${cron2}")
    public void sendMail() {
        try {
            String s = HttpClientUtils.doGet(url, null);
            ConvertibleBondResult convertibleBondResult = JSONObject.parseObject(s, ConvertibleBondResult.class);
            List<ConvertibleBondItem> rows = convertibleBondResult.getRows();
            if (CollectionUtils.isEmpty(rows)) {
                log.error("rows is empty");
                return;
            }

//            String today = "2020-03-31";
            StringBuilder stringBuilder = new StringBuilder();
            ArrayList<String> dateList = getDateList(7);
            String title = null;
            for (int i = 0; i < dateList.size(); i++) {
                String bondInfoByDate = getBondInfoByDate(rows, dateList.get(i));
                stringBuilder.append(bondInfoByDate).append("\n");

                if (i == 0) {
                    if (bondInfoByDate.contains("非工作日")) {
                        title = bondInfoByDate;
                    } else if (bondInfoByDate.contains("没债")) {
                        title = bondInfoByDate;
                    } else {
                        title = "今日有债打哦";
                    }
                }
            }


            String mailContent = stringBuilder.toString();
            title = title.replaceAll("\\n"," ");
            log.info("title:{},mail content:{}", title, mailContent);
            mailContent = mailContent.replaceAll("\\n", "<br>");
            System.out.println(mailContent);
            EmailTest.sendEmail(mailContent,title);
        } catch (Exception e) {
            log.error("e:{}", e);
        }
//
//        ResponseEntity<Object> entity = new RestTemplate().getForEntity(url, null, Object.class);
//        Object body = entity.getBody();
    }

    private ArrayList<String> getDateList(int count) {
        ArrayList<String> dateList = new ArrayList<>();
        DateTime now = DateTime.now();
        for (int i = 0; i < count; i++) {
            dateList.add(now.plusDays(i).toString("yyyy-MM-dd"));
        }
        return dateList;
    }

    private String getBondInfoByDate(List<ConvertibleBondItem> rows, String today) {
        StringBuilder stringBuilder = new StringBuilder();
        if (DateUtils.isRest(today)) {
            stringBuilder.append(today).append(":\n").append("非工作日哦,小宝宝").append("\n");
            return stringBuilder.toString();
        }

        ArrayList<ConvertibleBondItemCell> list = new ArrayList<>();
        for (ConvertibleBondItem row : rows) {
            ConvertibleBondItemCell cell = row.getCell();
            if (Objects.equals(cell.getApply_date(), today)) {
                list.add(cell);
            }
        }

        //转换一下
        ArrayList<ConvertibleBondItemVOForSendMail> sendMailArrayList = new ArrayList<>();
        for (ConvertibleBondItemCell cell : list) {
            ConvertibleBondItemVOForSendMail vo = new ConvertibleBondItemVOForSendMail();
            vo.setBond_id(cell.getBond_id());
            vo.setBond_nm(cell.getBond_nm());
            vo.setApply_date(cell.getApply_date());
            vo.setRating_cd(cell.getRating_cd());
            vo.setJsl_advise_text(cell.getJsl_advise_text());
            vo.setProgress_nm(cell.getProgress_nm());


            sendMailArrayList.add(vo);
        }

        if (CollectionUtils.isEmpty(sendMailArrayList)) {
            stringBuilder.append(today).append(":\n").append("本日没债哦,小宝宝").append("\n");
        } else {
            for (ConvertibleBondItemVOForSendMail sendMail : sendMailArrayList) {
                stringBuilder.append("小宝宝，").append(today).append(":\n").append("可申购的可转债如下：\n").append("可转债id:").append(sendMail.getBond_id()).append("\n")
                        .append("可转债名称:").append(sendMail.getBond_nm()).append("\n")
                        .append("评级：").append(sendMail.getRating_cd()).append("\n")
                        .append("申购建议：").append(sendMail.getJsl_advise_text()).append("\n")
                        .append("方案进展：").append(sendMail.getProgress_nm()).append("\n").append("\n");
            }
        }

        return stringBuilder.toString();
    }



    public static void main(String[] args) throws IOException {
        new CheckPendingDisposalCountTask().sendMail();
//        System.out.println(s);
    }
}
