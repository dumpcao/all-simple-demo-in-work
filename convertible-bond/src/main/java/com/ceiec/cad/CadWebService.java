package com.ceiec.cad;

import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Hello world!
 */
@SpringBootApplication
@ComponentScan("com.ceiec")
//@Slf4j
@EnableScheduling
public class CadWebService {

    public static void main(String[] args) {
        new SpringApplicationBuilder(CadWebService.class).web(WebApplicationType.NONE).run(args);

    }
    
}
