
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;
import org.xunche.agent.TimeAgent;
import org.xunche.agent.TimeAgentByJava;
import org.xunche.agent.TimeClassVisitor;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class SaveGeneratedClassWithOriginAgentTest {

    public static void main(String[] args) throws IOException {
        ClassReader reader = new ClassReader("org.xunche.app.HelloXunChe");
        ClassWriter writer = new ClassWriter(reader, ClassWriter.COMPUTE_FRAMES | ClassWriter.COMPUTE_MAXS);
        reader.accept(new TimeAgentByJava.TimeClassVisitor(writer), ClassReader.EXPAND_FRAMES);
        byte[] bytes = writer.toByteArray();

        File file = new File(
"F:\\ownprojects\\all-simple-demo-in-work\\java-agent-premain-demo\\test-agent\\src\\main\\java\\org\\xunche\\app\\HelloXunCheCopy2.class");
        FileOutputStream fos = new FileOutputStream(file);
        fos.write(bytes);
        fos.close();
    }
}
