package org.xunche.app;
public class HelloXunChe {
    private String methodName = "abc";
    public static void main(String[] args) throws InterruptedException {
        HelloXunChe helloXunChe = new HelloXunChe();
        helloXunChe.sayHi();
    }
    public void sayHi() throws InterruptedException {
        System.out.println("hi, xunche");
        sleep();
    }

    public void sleep() throws InterruptedException {
        Thread.sleep((long) (Math.random() * 200));
    }

}