import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationConfig;

import java.util.Date;
import java.util.TimeZone;

/**
 * desc:
 *
 * @author : ckl
 * creat_date: 2019/12/27 0027
 * creat_time: 13:36
 **/
public class Test {

    @org.junit.Test
    public void normal() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        TestVO vo = new TestVO();
        vo.setTestDate(new Date());
        String value = mapper.writeValueAsString(vo);
        System.out.println(value);
    }


    @org.junit.Test
    public void specifyDefaultTimezone() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        SerializationConfig oldSerializationConfig = mapper.getSerializationConfig();
        /**
         * 新的序列化配置，要配置时区
         */
        String timeZone = "GMT+8";
        SerializationConfig newSerializationConfig = oldSerializationConfig.with(TimeZone.getTimeZone(timeZone));

        mapper.setConfig(newSerializationConfig);
        TestVO vo = new TestVO();
        vo.setTestDate(new Date());
        String value = mapper.writeValueAsString(vo);
        System.out.println(value);
    }



    @org.junit.Test
    public void specifyTimezoneOnField() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        TestVoWithTimeZone vo = new TestVoWithTimeZone();
        vo.setTestDate(new Date());
        String value = mapper.writeValueAsString(vo);
        System.out.println(value);
    }
}
