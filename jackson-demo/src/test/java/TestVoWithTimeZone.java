import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * desc:
 *
 * @author : ckl
 * creat_date: 2019/12/27 0027
 * creat_time: 12:50
 **/
@Data
public class TestVoWithTimeZone {
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date testDate;
}
