package com.example.webdemo;

import com.example.webdemo.service.IBusinessService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * desc:
 *
 * @author : ckl
 * creat_date: 2019/12/12 0012
 * creat_time: 8:51
 **/
@RestController
@Slf4j
public class BusinessController {
    @Autowired
    private IBusinessService iBusinessService;

    @GetMapping("test.do")
    public void test(){
        iBusinessService.doBusiness("A");
        log.info("test b........");
        iBusinessService.doBusiness("b");
    }
}
