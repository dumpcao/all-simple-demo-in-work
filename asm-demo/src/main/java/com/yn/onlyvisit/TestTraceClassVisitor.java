package com.yn.onlyvisit;


import org.objectweb.asm.ClassReader;
import org.objectweb.asm.util.ASMifier;
import org.objectweb.asm.util.Textifier;
import org.objectweb.asm.util.TraceClassVisitor;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class TestTraceClassVisitor {
    public static void main(String[] args) throws IOException {
        /**
         * 方式1：打印生成指定类，需要的asm代码
         */
        ASMifier.main(new String[]{"com.yn.onlyvisit.Person"});

        /**
         * 方式2：生成要遍历的class的字节码
         */
        ClassReader classReader = new ClassReader("com.yn.onlyvisit.Person");
        PrintWriter printWriter = new PrintWriter(System.out);
        TraceClassVisitor classVisitor = new TraceClassVisitor(printWriter);
        classReader.accept(classVisitor,ClassReader.SKIP_DEBUG);

        /**
         * 方式3：生成要遍历的class的字节码
         */
        Textifier.main(new String[]{"com.yn.onlyvisit.Person"});
    }
}
