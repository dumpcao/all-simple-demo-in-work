package com.yn.onlyvisit;


import org.objectweb.asm.*;
import org.objectweb.asm.commons.AdviceAdapter;
import org.objectweb.asm.commons.AnalyzerAdapter;
import org.objectweb.asm.util.ASMifier;
import org.objectweb.asm.util.Textifier;
import org.objectweb.asm.util.TraceMethodVisitor;

import java.util.ArrayList;
import java.util.List;

public class MyClassVistor extends ClassVisitor {
    private List<String> methodList =  new ArrayList<>();
    private List<String> annotationOnClass =  new ArrayList<>();
    public MyClassVistor() {
        super(Opcodes.ASM6);
    }


    @Override
    public MethodVisitor visitMethod(int access, String name,
                                     String desc, String signature,
                                     String[] exceptions) {
        System.out.println("visitMethod: " + name);
        methodList.add(name);

        return super.visitMethod(access, name, desc, signature, exceptions);
    }

    @Override
    public AnnotationVisitor visitAnnotation(String descriptor, boolean visible) {
        annotationOnClass.add(descriptor);
        return super.visitAnnotation(descriptor, visible);
    }

    @Override
    public FieldVisitor visitField(int access, String name, String descriptor, String signature, Object value) {
        System.out.println("field:" + name);
        return super.visitField(access, name, descriptor, signature, value);
    }

    public List<String> getMethodList() {
        return methodList;
    }

    public List<String> getAnnotationOnClass() {
        return annotationOnClass;
    }
}
