package com.yn.onlyvisit;


import org.objectweb.asm.ClassReader;

import java.io.IOException;
import java.util.List;

public class TestClassVisit {
    public static void main(String[] args) throws IOException {
        ClassReader classReader = new ClassReader("com.yn.onlyvisit.Person");
        MyClassVistor classVisitor = new MyClassVistor();
        classReader.accept(classVisitor,ClassReader.SKIP_DEBUG);
        List<String> methodList = classVisitor.getMethodList();
        System.out.println(methodList);
        System.out.println(classVisitor.getAnnotationOnClass());
    }
}
