package com.yn.classgenerate;


import org.objectweb.asm.ClassVisitor;

import static org.objectweb.asm.Opcodes.*;

public class ChangeVersionAdapter extends ClassVisitor {

    public ChangeVersionAdapter(ClassVisitor classVisitor) {
        super(ASM4, classVisitor);
    }

    @Override
    public void visit(int version, int access, String name,
                      String signature, String superName, String[] interfaces) {
        cv.visit(V1_8, access, name, signature, superName, interfaces);
    }

}
