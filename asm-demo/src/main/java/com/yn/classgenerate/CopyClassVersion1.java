package com.yn.classgenerate;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import static org.objectweb.asm.Opcodes.ASM4;

public class CopyClassVersion1 {
    public static void main(String[] args) throws IOException {
        ClassReader classReader = new ClassReader("com.yn.classgenerate.CopyClass");

        ClassWriter cw = new ClassWriter(0);
        classReader.accept(cw, 0);
        byte[] b2 = cw.toByteArray();

        File file = new File("F:\\gitee-ckl\\all-simple-demo-in-work\\asm-demo\\src\\main\\java\\com\\yn\\classgenerate\\CopyClass2.class");
        FileOutputStream fos = new FileOutputStream(file);
        fos.write(b2);
        fos.close();
    }
}
