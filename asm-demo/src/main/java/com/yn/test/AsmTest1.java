package com.yn.test;


import org.objectweb.asm.*;
import org.objectweb.asm.commons.AdviceAdapter;
import org.objectweb.asm.util.ASMifier;
import org.objectweb.asm.util.Textifier;
import org.objectweb.asm.util.TraceMethodVisitor;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;


public class AsmTest1 {
    public static void main(String[] args) throws IOException {
//        ClassReader classReader = new ClassReader("com.yn.test.Test");
        ClassReader classReader = new ClassReader("java.lang.String");
        ClassVisitor classVisitor = new TestClassVisitor();
        classReader.accept(classVisitor, ClassReader.SKIP_DEBUG);
    }

    private static class TestClassVisitor extends ClassVisitor {
        public TestClassVisitor() {
            super(Opcodes.ASM6);
        }

        @Override
        public MethodVisitor visitMethod(int access, String name,
                                         String desc, String signature,
                                         String[] exceptions) {
            System.out.println("visitMethod: " + name);
//            if ("main".equals(name)) {
            if (true) {
                Textifier textifier = new Textifier();
                ASMifier asMifier = new ASMifier();
//                TraceMethodVisitor visitor = new TraceMethodVisitor(textifier);
                TraceMethodVisitor visitor = new TraceMethodVisitor(asMifier);
//                MethodVisitor mv = super.visitMethod(access, name, desc, signature, exceptions);
//                return new MainMethodAdapter(mv,access,name,desc);
                return visitor;
            }
            return null;
//            if ("<init>".equals(name)) {
////                mv = new MainMethodVisitor(mv);
//                mv = new MainMethodAdapter(mv,access,name,desc);
//            }
//            return mv;
        }


        @Override
        public FieldVisitor visitField(int access, String name, String descriptor, String signature, Object value) {
            System.out.println("field:" + name);
            return super.visitField(access, name, descriptor, signature, value);
        }
    }


}
