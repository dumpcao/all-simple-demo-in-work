package com.yn.test;


import com.yn.sample.cr.AsmAnnotationVisitor;
import org.objectweb.asm.*;
import org.objectweb.asm.util.Textifier;
import org.objectweb.asm.util.TraceMethodVisitor;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;


public class AsmTest {
    public static void main(String[] args) throws IOException {
        ClassReader classReader = new ClassReader("com.yn.test.Test");
        ClassWriter classWriter = new ClassWriter(ClassWriter.COMPUTE_MAXS);
        ClassVisitor classVisitor = new TestClassVisitor(classWriter);
        classReader.accept(classVisitor, ClassReader.SKIP_DEBUG);

        byte[] classFile = classWriter.toByteArray();
//        File file = new File("D:\\用户目录\\下载\\AsmButterknife-\\sample-java\\build\\classes\\java\\main\\com\\yn\\test\\Test.class");
        File file = new File("E:\\testclass.txt");
        FileOutputStream fos = new FileOutputStream(file);
        fos.write(classFile);
        fos.close();
    }

    private static class TestClassVisitor extends ClassVisitor {

        public TestClassVisitor(ClassVisitor classVisitor) {
            super(Opcodes.ASM6, classVisitor);
        }

        @Override
        public MethodVisitor visitMethod(int access, String name,
                                         String desc, String signature,
                                         String[] exceptions) {
            System.out.println("visitMethod: " + name);
            Textifier textifier = new Textifier();
            TraceMethodVisitor visitor = new TraceMethodVisitor(textifier);
            return visitor;
        }

        @Override
        public AnnotationVisitor visitAnnotation(String descriptor, boolean visible) {
            System.out.println("desc:" + descriptor);
            return new AsmAnnotationVisitor(Opcodes.ASM6,null);
        }
    }

    private static class MainMethodVisitor extends MethodVisitor {

        public MainMethodVisitor(MethodVisitor methodVisitor) {
            super(Opcodes.ASM6, methodVisitor);
        }

        private void sop(String msg) {
            mv.visitFieldInsn(Opcodes.GETSTATIC,
                    Type.getInternalName(System.class), //"java/lang/System"
                    "out",
                    Type.getDescriptor(PrintStream.class) //"Ljava/io/PrintStream;"
            );
            mv.visitLdcInsn(msg);
            mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL,
                    Type.getInternalName(PrintStream.class),
                    "println",
                    "(Ljava/lang/String;)V",
                    false);
        }

        @Override
        public void visitCode() {
            mv.visitCode();
            System.out.println("method start to insert code");
            sop("asm insert before");
        }

        @Override
        public void visitInsn(int opcode) {
            if (opcode == Opcodes.RETURN) {
                System.out.println("method end to insert code");
                sop("asm insert after");
            }
            mv.visitInsn(opcode);
        }
    }


}
