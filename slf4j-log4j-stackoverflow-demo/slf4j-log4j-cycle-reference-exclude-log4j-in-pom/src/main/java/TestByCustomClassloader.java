import org.apache.log4j.Logger;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * desc:
 *
 * @author : ckl
 * creat_date: 2019/12/26 0026
 * creat_time: 18:45
 **/
public class TestByCustomClassloader {

    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, InstantiationException, MalformedURLException, ClassNotFoundException {
        org.apache.log4j.Logger logger = Logger.getLogger("test");
        System.out.println(logger);
    }
}
