import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.misc.Launcher;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;

/**
 * desc:
 *
 * @author : ckl
 * creat_date: 2019/12/26 0026
 * creat_time: 18:45
 **/
public class Test {

    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, InstantiationException {
        Constructor<org.apache.log4j.Logger> constructor = org.apache.log4j.Logger.class.getDeclaredConstructor(String.class);
        constructor.setAccessible(true);
        org.apache.log4j.Logger logger = constructor.newInstance("test");
//        Logger logger = LoggerFactory.getLogger(Test.class);
//        logger.info("hahha");
        ClassLoader loader = Test.class.getClassLoader();
        Method getURLs = loader.getClass().getMethod("getURLs");
        getURLs.setAccessible(true);
        URL[] o = (URL[]) getURLs.invoke(loader);
        for (URL url : o) {
            System.out.println(url);
        }
    }
}
