import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * desc:
 *
 * @author : ckl
 * creat_date: 2019/12/26 0026
 * creat_time: 18:45
 **/
public class TestByCustomClassloader {

    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, InstantiationException, MalformedURLException, ClassNotFoundException {
        URL[] urls = new URL[1];
        URL url1 = new URL("file:/D:/soft/Repo_backup/org/slf4j/log4j-over-slf4j/1.7.24/log4j-over-slf4j-1.7.24.jar");
        urls[0] = url1;
        MyClassLoader myClassLoader = new MyClassLoader(urls);
        Class<?> aClass = myClassLoader.loadClass("org.apache.log4j.Logger");
        Constructor constructor = aClass.getDeclaredConstructor(String.class);
        constructor.setAccessible(true);
        Object logger = constructor.newInstance("test");
        System.out.println(logger);
//        Logger logger = LoggerFactory.getLogger(Test.class);
//        logger.info("hahha");
        ClassLoader loader = TestByCustomClassloader.class.getClassLoader();
        Method getURLs = loader.getClass().getMethod("getURLs");
        getURLs.setAccessible(true);
        URL[] o = (URL[]) getURLs.invoke(loader);
        for (URL url : o) {
            System.out.println(url);
        }
    }
}
