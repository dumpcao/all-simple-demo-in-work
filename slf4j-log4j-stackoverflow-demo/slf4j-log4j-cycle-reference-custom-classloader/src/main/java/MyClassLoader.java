import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.net.URL;
import java.net.URLClassLoader;

/**
 * desc:
 *
 * @author : ckl
 * creat_date: 2019/12/26 0026
 * creat_time: 20:17
 **/
public class MyClassLoader extends URLClassLoader {
    private String classPath;
    private String className;

    public MyClassLoader(URL[] urls) {
        super(urls);
    }

    @Override
    public Class<?> loadClass(String name) throws ClassNotFoundException {
        try {
            Class<?> clazz = findClass(name);
            return clazz;
        } catch (Exception e) {
            return super.loadClass(name);
        }

    }
}
