spring boot应用集成logstash appender，日志会直接发送到logstash。
格式可自定义，我这边这样定义后，发送出去的格式如下（可抓包查看）：
```json
{
  "@timestamp": "2019-12-13T09:03:50.774+00:00",
  "severity": "INFO",
  "service": "springAppName_IS_UNDEFINED",
  "trace": "",
  "span": "",
  "exportable": "",
  "pid": "19472",
  "thread": "http-nio-8082-exec-1",
  "class": "com.example.webdemo.BusinessController",
  "rest": "test b........"
}
```

参考https://www.cnblogs.com/ljw-bim/p/9606572.html