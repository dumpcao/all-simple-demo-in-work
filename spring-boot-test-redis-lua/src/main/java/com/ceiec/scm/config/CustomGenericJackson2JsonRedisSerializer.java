package com.ceiec.scm.config;

import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.SerializationException;

public class CustomGenericJackson2JsonRedisSerializer extends GenericJackson2JsonRedisSerializer {

    @Override
    public byte[] serialize(Object source) throws SerializationException {
//        if (source instanceof byte[]) {
//            return (byte[]) source;
//        }
        return super.serialize(source);
    }
}
