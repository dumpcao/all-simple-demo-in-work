package com.ceiec.scm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;

import java.net.InetAddress;
import java.net.UnknownHostException;

@SpringBootApplication
@Controller
@ComponentScan("com.ceiec")
@EnableAutoConfiguration
public class BootstrapApplication {




    public static void main(String[] args) throws UnknownHostException {
        ConfigurableApplicationContext application =SpringApplication.run(BootstrapApplication.class, args);
        Environment env = application.getEnvironment();
        String ip = InetAddress.getLocalHost().getHostAddress();
        String port = env.getProperty("server.port");
        String path = env.getProperty("server.servlet.context-path");
        if(StringUtils.isEmpty(path)){
            path="";
        }
        String profile = (env.getActiveProfiles() != null && env.getActiveProfiles().length > 0) ? env.getActiveProfiles()[0] : null;

    }

}
