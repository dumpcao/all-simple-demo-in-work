package com.ceiec.scm.vo;

public class TestVO {
    private int count;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
