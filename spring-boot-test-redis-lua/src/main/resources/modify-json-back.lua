redis.call('select',1);
local key = KEYS[1]
local hashfield = ARGV[1]
local fieldValue=redis.call('hget', key, hashfield)
local vo = cjson.decode(fieldValue);

--- get old value of specified key, increment it
local fieldNameOfFieldValue = ARGV[2];
vo[fieldNameOfFieldValue] = vo[fieldNameOfFieldValue] + 1;

--- encode and set it back
local resultVo = cjson.encode(vo);
redis.call('hset', key, hashfield, resultVo)


return redis.call('hget', key, hashfield)