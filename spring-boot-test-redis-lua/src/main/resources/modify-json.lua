local key = KEYS[1]
local hashfield = ARGV[1]
local jsonPropertyToModify = ARGV[2]
local reply = redis.call('select',10);
local fieldValue=redis.call('hget', key, hashfield);

local vo = cjson.decode(fieldValue)
vo[jsonPropertyToModify] = vo[jsonPropertyToModify] + 1;

--- encode and set it back
local resultVo = cjson.encode(vo);
redis.call('hset', key, hashfield, resultVo)
return redis.call('hget', key, hashfield);