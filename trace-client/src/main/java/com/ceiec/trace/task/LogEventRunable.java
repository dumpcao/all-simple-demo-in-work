package com.ceiec.trace.task;

import com.ceiec.trace.utils.HttpClientUtils;

/**
 * desc:
 *
 * @author : caokunliang
 * creat_date: 2018/10/24 0024
 * creat_time: 15:20
 **/
public class LogEventRunable implements Runnable {

    private String url;

    private String content;

    public LogEventRunable(String url, String content) {
        this.url = url;
        this.content = content;
    }

    @Override
    public void run() {
        try {
            HttpClientUtils.doPost(url,content);
        } catch (Exception e) {
        }
    }
}
