package com.ceiec.trace.vo;

import lombok.Data;

import java.util.Date;

/**
 * desc:
 *
 * @author : caokunliang
 * creat_date: 2018/10/24 0024
 * creat_time: 14:32
 **/
@Data
public class PeerToPeerRequestTrace {
    /**
     * traceId
     */
    private String traceId;

    /**
     * 发起请求的源主机
     */
    private String srcHost;


    /**
     * 发起请求的目的主机
     */
    private String destHost;

    /**
     * 发起请求的目的端口
     */
    private String destPort;

    /**
     * 发起请求的目的app名
     */
    private String destApplication;

    /**
     * 发起请求的源端口
     */
    private String srcPort;

    /**
     * 发起请求的app名
     */
    private String srcApplication;

    /**
     * 请求的url
     */
    private String requestUrl;

    /**
     * 请求参数，以json表示
     */
    private String requestParam;

    /**
     * 响应内容
     */
    private String responseContent;

    /**
     * 发起请求的时间，以当前服务器时间为准
     */
    private Date requestTime;


    /**
     * 收到响应的时间，以当前服务器时间为准
     */
    private Date responseTime;

    /**
     * 该请求花费的时间
     */
    private Long spentTime;

}
