package com.ceiec.trace.vo;

import lombok.Data;

/**
 * desc:
 *
 * @author : caokunliang
 * creat_date: 2018/9/12 0012
 * creat_time: 11:18
 **/
@Data
public class LoggingEvent {
    private String traceId;

    private String destIp;

    private String appName;

    private Long timestamp;

    private String logContent;

    private String destPort;


}
