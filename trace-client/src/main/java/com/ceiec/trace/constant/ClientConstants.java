package com.ceiec.trace.constant;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * desc:
 *
 * @author : caokunliang
 * creat_date: 2018/10/24 0024
 * creat_time: 15:13
 **/
public class ClientConstants {
     public static ExecutorService executorService =
     Executors.newSingleThreadExecutor();
}
