package com.ceiec.trace.filter;

import com.ceiec.trace.vo.CurrentRequestMetaData;

/**
 * desc:
 *
 * @author : caokunliang
 * creat_date: 2018/10/25 0025
 * creat_time: 13:59
 **/
public class RequestHolder {
    public final static ThreadLocal<CurrentRequestMetaData> requestMetaDataThreadLocal =
            new ThreadLocal<>();

    public static CurrentRequestMetaData get(){
        return requestMetaDataThreadLocal.get();
    }

    public static void  set(CurrentRequestMetaData data){
        requestMetaDataThreadLocal.set(data);
    }

    public static void remove(){
        requestMetaDataThreadLocal.remove();
    }
}
