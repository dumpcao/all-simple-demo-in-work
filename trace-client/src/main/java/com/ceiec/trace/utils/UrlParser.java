package com.ceiec.trace.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * desc:
 *
 * @author : caokunliang
 * creat_date: 2018/10/25 0025
 * creat_time: 11:44
 **/
public class UrlParser {
    private static Pattern pattern = Pattern.compile("http://([a-zA-Z0-9.]+):([0-9]{2,5})/([\\w_]+).*");

    public static IpPortContextVO getIpPortContextVO(CharSequence url){
        Matcher matcher = pattern.matcher(url);
        boolean b = matcher.find();
        if(b){
            String hostName = matcher.group(1);
            String port = matcher.group(2);
            String contextPath = matcher.group(3);

            IpPortContextVO vo = new IpPortContextVO();
            vo.setHost(hostName);
            vo.setPort(port);
            vo.setContext(contextPath);

            return vo;
        }

        return null;
    }
}
